<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it" sourcelanguage="en">
  <context>
    <name>MainMenu</name>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="308"/>
      <source>Settings</source>
      <translation>Impostazioni</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="232"/>
      <source>Slideshow</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Presentazione</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="86"/>
      <source>Rename file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Rinomina file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="91"/>
      <source>Copy file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Copia file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="96"/>
      <source>Move file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Sposta file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="101"/>
      <source>Delete file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Elimina file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="114"/>
      <source>Tag faces</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Etichetta facce</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Hide metadata</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Nascondi metadati</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Show metadata</source>
      <translation>Mostra metadati</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Hide histogram</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Nascondi istogramma</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Show histogram</source>
      <translation>Mostra istogramma</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="124"/>
      <source>Set as wallpaper</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Imposta come sfondo</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="322"/>
      <source>Quit</source>
      <translation>Esci</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="119"/>
      <source>Scale image</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Scala immagine</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="173"/>
      <source>Navigation</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Navigazione</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="177"/>
      <source>Browse images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Sfoglia immagini</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="181"/>
      <source>first</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>prima</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="184"/>
      <source>last</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>ultima</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="193"/>
      <source>Zoom</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Ingrandimento</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="200"/>
      <source>Fit</source>
      <extracomment>This is an entry in the main menu on the right, used as in &apos;FIT image in window (reset zoom)&apos;. Please keep short!</extracomment>
      <translation>Adatta</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="208"/>
      <source>Rotation/Flip</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Rotazione/Riflessione</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="212"/>
      <source>Horizontal flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Riflessione orizzontale</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="216"/>
      <source>Vertical flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Riflessione verticale</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="220"/>
      <source>Reset flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Resetta riflessione</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="224"/>
      <source>Reset rotation</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Resetta rotazione</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="236"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="249"/>
      <source>Start</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;START slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation>Avvia</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="240"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="253"/>
      <source>Setup</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;SETUP slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation>Configurazione</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="245"/>
      <source>Advanced Sort</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Ordinamento avanzato</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="258"/>
      <source>Other</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Altro</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="261"/>
      <source>Filter images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Filtra immagini</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="276"/>
      <source>External</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Esterno</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="311"/>
      <source>About</source>
      <translation>Informazioni</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="319"/>
      <source>Online help</source>
      <translation>Guida in linea</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="124"/>
      <source>Click and drag to resize main menu</source>
      <translation>Clicca e trascina per ridimensionare il menu principale</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="265"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Streaming (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="109"/>
      <source>Copy to clipboard</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Copia negli appunti</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="269"/>
      <source>Open in default file manager</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Aprire con il gestore di file predefinito</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuPopout.qml" line="34"/>
      <source>Main Menu</source>
      <extracomment>Window title</extracomment>
      <translation>Menu Principale</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="100"/>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="205"/>
      <source>You need to load an image first.</source>
      <translation>È necessario caricare un'immagine prima.</translation>
    </message>
  </context>
  <context>
    <name>PQImageFormats</name>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="48"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="60"/>
      <source>ERROR getting default image formats</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERRORE impossibile recuperare i formati immagine predefiniti</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the database of default image formats.</source>
      <translation>Ce l'ho messa tutta, ma non riesco proprio ad aprire nemmeno una versione in sola lettura del database dei formati immagine predefiniti.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Da qualche parte, qualcosa è andato terribilmente storto!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default image formats.</source>
      <translation>Ce l'ho messa tutta, ma non riesco proprio ad aprire il database dei formati immagine predefiniti.</translation>
    </message>
  </context>
  <context>
    <name>PQMetaData</name>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="421"/>
      <source>yes</source>
      <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
      <translation>sì</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="423"/>
      <source>no</source>
      <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
      <translation>no</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="425"/>
      <source>No flash function</source>
      <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
      <translation>Nessuna funzione flash</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="427"/>
      <source>strobe return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luce strobo di ritorno non rilevata</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="429"/>
      <source>strobe return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luce strobo di ritorno rilevata</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="431"/>
      <source>compulsory flash mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modalità flash sempre attivo</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="433"/>
      <source>auto mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modalità automatica</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="435"/>
      <source>red-eye reduction mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>modalità riduzione occhi rossi</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="437"/>
      <source>return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luce di ritorno rilevata</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="439"/>
      <source>return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>luce di ritorno non rilevata</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="487"/>
      <source>Invalid flash</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Flash non valido</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="498"/>
      <source>Standard</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Standard</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="501"/>
      <source>Landscape</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Orizzontale</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="504"/>
      <source>Portrait</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Verticale</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="507"/>
      <source>Night Scene</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Scena Notturna</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="510"/>
      <source>Invalid Scene Type</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Tipo Di Scena Non Valido</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="563"/>
      <source>Unknown</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Sconosciuto</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="566"/>
      <source>Daylight</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luce diurna</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="569"/>
      <source>Fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="572"/>
      <source>Tungsten (incandescent light)</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tungsteno (luce a incandescenza)</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="575"/>
      <source>Flash</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Flash</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="578"/>
      <source>Fine weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Bel tempo</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="581"/>
      <source>Cloudy Weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Nuvoloso</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="584"/>
      <source>Shade</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Ombra</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="587"/>
      <source>Daylight fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luce diurna fluorescente</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="590"/>
      <source>Day white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente bianca diurna</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="593"/>
      <source>Cool white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente bianca fredda</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="596"/>
      <source>White fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluorescente bianca</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="599"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="602"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="605"/>
      <source>Standard light</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Luce standard</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="618"/>
      <source>Other light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Altra sorgente luminosa</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="621"/>
      <source>Invalid light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Sorgente luminosa non valida</translation>
    </message>
  </context>
  <context>
    <name>PQPrintSupport</name>
    <message>
      <location filename="../cplusplus/print/printsupport.cpp" line="52"/>
      <source>Print</source>
      <translation>Stampa</translation>
    </message>
  </context>
  <context>
    <name>PQSettings</name>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="64"/>
      <source>ERROR getting database with default settings</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERRORE nel recupero del database con le impostazioni predefinite</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="65"/>
      <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
      <translation>Ce l'ho messa tutta, ma non riesco proprio ad aprire nemmeno una versione in sola lettura del database delle impostazioni.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="65"/>
      <location filename="../cplusplus/settings/settings.cpp" line="77"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Da qualche parte, qualcosa è andato terribilmente storto!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="76"/>
      <source>ERROR opening database with default settings</source>
      <translation>ERRORE nell'apertura del database con le impostazioni predefinite</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="77"/>
      <source>I tried hard, but I just cannot open the database of default settings.</source>
      <translation>Ce l'ho messa tutta, ma non riesco proprio ad aprire il database delle impostazioni predefinite.</translation>
    </message>
  </context>
  <context>
    <name>PQShortcuts</name>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="48"/>
      <source>ERROR getting database with default shortcuts</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>ERRORE nel recupero del database con le scorciatoie predefinite</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
      <translation>Ce l'ho messa tutta, ma non riesco proprio ad aprire nemmeno una versione in sola lettura del database delle scorciatoie.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Da qualche parte, qualcosa è andato terribilmente storto!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="60"/>
      <source>ERROR opening database with default settings</source>
      <translation>ERRORE nell'apertura del database con le impostazioni predefinite</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
      <translation>Ce l'ho messa tutta, ma non riesco proprio ad aprire il database delle scorciatoie predefinite.</translation>
    </message>
  </context>
  <context>
    <name>PQStartup</name>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="56"/>
      <source>SQLite error</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>Errore di SQLite</translation>
    </message>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="57"/>
      <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
      <translation>Sembra che ti manchi il driver SQLite per Qt. È necessario per diverse cose, come leggere e scrivere le impostazioni. Senza di esso, PhotoQt non può funzionare!</translation>
    </message>
  </context>
  <context>
    <name>TabShortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/TabShortcuts.qml" line="245"/>
      <source/>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>about</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="52"/>
      <location filename="../qml/about/PQAbout.qml" line="165"/>
      <source>Close</source>
      <translation>Chiudi</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="98"/>
      <source>Current version:</source>
      <translation>Versione attuale:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="100"/>
      <source>Show configuration overview</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Mostra panoramica della configurazione</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="110"/>
      <source>License:</source>
      <translation>Licenza:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="115"/>
      <source>Open license in browser</source>
      <translation>Apri licenza nel browser</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="135"/>
      <source>Open website in browser</source>
      <translation>Apri sito web nel browser</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="203"/>
      <source>Configuration</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Configurazione</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="222"/>
      <source>Copy to clipboard</source>
      <translation>Copia negli appunti</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="130"/>
      <source>Website:</source>
      <translation>Sito web:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="145"/>
      <source>Contact:</source>
      <translation>Contatti:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="150"/>
      <source>Send an email</source>
      <translation>Invia una e-mail</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAboutPopout.qml" line="34"/>
      <source>About</source>
      <extracomment>Window title</extracomment>
      <translation>Informazioni</translation>
    </message>
  </context>
  <context>
    <name>advancedsort</name>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="83"/>
      <location filename="../qml/advancedsort/PQAdvancedSortPopout.qml" line="34"/>
      <source>Advanced Image Sort</source>
      <extracomment>Window title</extracomment>
      <translation>Ordinamento Immagini Avanzato</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="94"/>
      <source>It is possible to sort the images in the current folder by color properties. Depending on the number of images and the settings, this might take a few seconds.</source>
      <translation>È possibile ordinare le immagini nella cartella corrente in base alle proprietà dei colori. A seconda del numero di immagini e delle impostazioni, potrebbe richiedere alcuni secondi.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="107"/>
      <source>Sort by:</source>
      <extracomment>Used as &apos;sort by dominant/average color&apos;</extracomment>
      <translation>Ordina per:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="116"/>
      <source>Resolution</source>
      <extracomment>The image resolution (width/height in pixels)</extracomment>
      <translation>Risoluzione</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="118"/>
      <source>Dominant color</source>
      <extracomment>The color that is most common in the image</extracomment>
      <translation>Colore dominante</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="120"/>
      <source>Average color</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Colore medio</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="122"/>
      <source>Luminosity</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Luminosità</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="137"/>
      <source>ascending</source>
      <extracomment>sort order, i.e., &apos;ascending order&apos;</extracomment>
      <translation>crescente</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="142"/>
      <source>descending</source>
      <extracomment>sort order, i.e., &apos;descending order&apos;</extracomment>
      <translation>decrescente</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="159"/>
      <source>speed vs quality:</source>
      <extracomment>Please keep short! Sorting images by color comes with a speed vs quality tradeoff.</extracomment>
      <translation>velocità vs qualità:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="165"/>
      <source>low quality (fast)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>bassa qualità (veloce)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="167"/>
      <source>medium quality</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>media qualità</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="169"/>
      <source>high quality (slow)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>alta qualità (lento)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="181"/>
      <source>There is also a quickstart shortcut that immediately starts the sorting using the latest settings.</source>
      <translation>C'è anche una scorciatoia di avvio rapido che avvia immediatamente l'ordinamento utilizzando le ultime impostazioni.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="201"/>
      <source>Sort images</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Ordina immagini</translation>
    </message>
  </context>
  <context>
    <name>buttongeneric</name>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="81"/>
      <source>Ok</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Ok</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="83"/>
      <source>Cancel</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Annulla</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="85"/>
      <source>Save</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Salva</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="87"/>
      <source>Close</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Chiudi</translation>
    </message>
  </context>
  <context>
    <name>commandlineparser</name>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="51"/>
      <source>Image Viewer</source>
      <translation>Visualizzatore immagini</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="53"/>
      <source>Image file to open.</source>
      <translation>File immagine da aprire.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="60"/>
      <source>Make PhotoQt ask for a new file.</source>
      <translation>Fai chiedere a PhotoQt un nuovo file.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="62"/>
      <source>Shows PhotoQt from system tray.</source>
      <translation>Mostra PhotoQt dal vassoio di sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="64"/>
      <source>Hides PhotoQt to system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Nasconde PhotoQt nel vassoio di sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="66"/>
      <source>Show/Hide PhotoQt.</source>
      <translation>Mostra/Nascondi PhotoQt.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="68"/>
      <source>Enable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Abilita miniature.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="70"/>
      <source>Disable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Disabilita miniature.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="72"/>
      <source>Enable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Abilita icona nel vassoio di sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="74"/>
      <source>Disable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Disabilita icona nel vassoio di sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="76"/>
      <source>Start PhotoQt hidden to the system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Avvia PhotoQt nascosto nel vassoio di sistema.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="78"/>
      <source>Open standalone PhotoQt, allows for multiple instances but without remote interaction.</source>
      <extracomment>Command line option</extracomment>
      <translation>Apri PhotoQt standalone, consente di avere più istanze ma senza interazione remota.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="80"/>
      <source>Simulate a shortcut sequence</source>
      <extracomment>Command line option</extracomment>
      <translation>Simula una sequenza di tasti come scorciatoia</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="82"/>
      <source>Switch on debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Attiva messaggi di debug.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="84"/>
      <source>Switch off debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Disattiva messaggi di debug.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="86"/>
      <source>Export configuration to given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Esporta la configurazione nel file con il nome dato.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="88"/>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="92"/>
      <source>filename</source>
      <extracomment>Command line option</extracomment>
      <translation>nome del file</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="90"/>
      <source>Import configuration from given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Importa la configurazione dal file con il nome dato.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="94"/>
      <source>Check the configuration and correct any detected issues.</source>
      <extracomment>Command line option</extracomment>
      <translation>Controllare la configurazione e correggere eventuali problemi rilevati.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="96"/>
      <source>Reset default configuration.</source>
      <extracomment>Command line option</extracomment>
      <translation>Ripristina la configurazione predefinita.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="98"/>
      <source>Show configuration overview.</source>
      <extracomment>Command line option</extracomment>
      <translation>Mostra panoramica della configurazione.</translation>
    </message>
  </context>
  <context>
    <name>facetagging</name>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="203"/>
      <source>Who is this?</source>
      <extracomment>This question is asked in the face tagger to ask for the name of a tagged face</extracomment>
      <translation>Chi è?</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="210"/>
      <source>Enter name</source>
      <translation>Inserisci nome</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="62"/>
      <source>Click to exit face tagging mode</source>
      <translation>Clicca per uscire dalla modalità di etichettatura facciale</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="75"/>
      <source>Click to tag faces, changes are saved automatically</source>
      <translation>Clicca per etichettare i volti, le modifiche vengono salvate automaticamente</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagsUnsupported.qml" line="60"/>
      <source>File type does not support face tags.</source>
      <translation>Il tipo di file non supporta le etichette facciali.</translation>
    </message>
  </context>
  <context>
    <name>filedialog</name>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="71"/>
      <source>Backwards</source>
      <translation>Indietro</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="98"/>
      <source>Up a level</source>
      <translation>Su di un livello</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="124"/>
      <source>Forwards</source>
      <translation>Avanti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="199"/>
      <source>List subfolders</source>
      <translation>Elenca sottocartelle</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="199"/>
      <source>No subfolders found</source>
      <translation type="unfinished">No subfolders found</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="289"/>
      <source>Enter fullscreen</source>
      <translation>Passa a schermo intero</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="290"/>
      <source>Exit fullscreen</source>
      <translation>Esci dalla modalità a schermo intero</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="319"/>
      <source>Close</source>
      <translation>Chiudi</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="109"/>
      <source>Storage devices</source>
      <extracomment>This is the category title of storage devices to open (like USB keys) in the element for opening files</extracomment>
      <translation>Dispositivi di archiviazione</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="141"/>
      <source>Detected storage devices on your system</source>
      <translation>Dispositivi di archiviazione rilevati nel tuo sistema</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="118"/>
      <source>no supported files/folders found</source>
      <translation>non sono stati trovati file o cartelle supportati</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="232"/>
      <source>Click and drag to favorites</source>
      <translation>Clicca e trascina nei preferiti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="360"/>
      <source># images</source>
      <translation>Numero di immagini</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="361"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="380"/>
      <source>Date:</source>
      <translation>Data:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="362"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="381"/>
      <source>Time:</source>
      <translation>Orario:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="378"/>
      <source>File size:</source>
      <translation>Dimensioni del file:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="379"/>
      <source>File type:</source>
      <translation>Tipo di file:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="463"/>
      <source>%1 image</source>
      <translation>%1 immagine</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="465"/>
      <source>%1 images</source>
      <translation>%1 immagini</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="51"/>
      <source>no folder added to favorites yet</source>
      <extracomment>&apos;favorites&apos; here refers to the list of favorite folders a user can set in the file dialog</extracomment>
      <translation>nessuna cartella aggiunta ai preferiti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="157"/>
      <source>Favorites</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Preferiti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="173"/>
      <source>Your favorites</source>
      <translation>I tuoi preferiti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Show entry</source>
      <translation>Mostra elemento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Hide entry</source>
      <translation>Nascondi elemento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="223"/>
      <source>Remove entry</source>
      <translation>Rimuovi elemento</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="224"/>
      <source>Hide hidden entries</source>
      <translation>Nascondi elementi nascosti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="224"/>
      <source>Show hidden entries</source>
      <translation>Mostra elementi nascosti</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this folder</source>
      <translation>Carica questa cartella</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this file</source>
      <translation type="unfinished">Load this file</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="56"/>
      <source>Add to Favorites</source>
      <translation type="unfinished">Add to Favorites</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="78"/>
      <source>Show tooltip with image details</source>
      <translation type="unfinished">Show tooltip with image details</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="88"/>
      <source>Visible</source>
      <extracomment>This is a context menu entry, referring to whether the large preview image is VISIBLE</extracomment>
      <translation type="unfinished">Visible</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="96"/>
      <source>Higher resolution</source>
      <extracomment>This is a context menu entry, referring to whether a preview image with a HIGHER RESOLUTION should be loaded</extracomment>
      <translation type="unfinished">Higher resolution</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="104"/>
      <source>Blurred image</source>
      <extracomment>This is a context menu entry, selecting it will BLUR the preview IMAGE</extracomment>
      <translation type="unfinished">Blurred image</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="112"/>
      <source>Muted colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image MUTED</extracomment>
      <translation type="unfinished">Muted colors</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="120"/>
      <source>Full colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image in FULL (i.e., without blur/opacity)</extracomment>
      <translation type="unfinished">Full colors</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="64"/>
      <source>Show hidden files</source>
      <translation type="unfinished">Show hidden files</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="71"/>
      <source>Show thumbnails</source>
      <translation type="unfinished">Show thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="103"/>
      <source>Standard</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation type="unfinished">Standard</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="119"/>
      <source>Some standard locations</source>
      <translation type="unfinished">Some standard locations</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="48"/>
      <source>Zoom:</source>
      <translation type="unfinished">Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="57"/>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="72"/>
      <source>Adjust font size of files and folders</source>
      <translation type="unfinished">Adjust font size of files and folders</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="73"/>
      <source>Zoom factor:</source>
      <translation type="unfinished">Zoom factor:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="92"/>
      <source>Sort by:</source>
      <translation type="unfinished">Sort by:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="94"/>
      <source>Name</source>
      <translation type="unfinished">Name</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="95"/>
      <source>Natural Name</source>
      <translation type="unfinished">Natural Name</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="96"/>
      <source>Time modified</source>
      <translation type="unfinished">Time modified</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="97"/>
      <source>File size</source>
      <translation type="unfinished">File size</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="98"/>
      <source>File type</source>
      <translation type="unfinished">File type</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="99"/>
      <source>reverse order</source>
      <translation type="unfinished">reverse order</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="106"/>
      <source>Choose by what to sort the files</source>
      <translation type="unfinished">Choose by what to sort the files</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="142"/>
      <source>All supported images</source>
      <translation type="unfinished">All supported images</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="147"/>
      <source>Video files</source>
      <translation type="unfinished">Video files</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="148"/>
      <source>All files</source>
      <translation type="unfinished">All files</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="158"/>
      <source>Choose which selection of files to show</source>
      <translation type="unfinished">Choose which selection of files to show</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="178"/>
      <source>Remember loaded folder between sessions.</source>
      <translation type="unfinished">Remember loaded folder between sessions.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="200"/>
      <source>Switch between list and icon view</source>
      <translation type="unfinished">Switch between list and icon view</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="230"/>
      <source>Open global settings</source>
      <translation type="unfinished">Open global settings</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Hide standard locations</source>
      <translation type="unfinished">Hide standard locations</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Show standard locations</source>
      <translation type="unfinished">Show standard locations</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Hide favorite locations</source>
      <translation type="unfinished">Hide favorite locations</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Show favorite locations</source>
      <translation type="unfinished">Show favorite locations</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Hide storage devices</source>
      <translation type="unfinished">Hide storage devices</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Show storage devices</source>
      <translation type="unfinished">Show storage devices</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialogPopout.qml" line="34"/>
      <source>File dialog</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">File dialog</translation>
    </message>
  </context>
  <context>
    <name>filemanagement</name>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="83"/>
      <location filename="../qml/filemanagement/PQDeletePopout.qml" line="34"/>
      <source>Delete file?</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Delete file?</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="101"/>
      <source>An error occured, file could not be deleted!</source>
      <translation type="unfinished">An error occured, file could not be deleted!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="121"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="177"/>
      <source>Move to trash</source>
      <translation type="unfinished">Move to trash</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="137"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="180"/>
      <source>Delete permanently</source>
      <translation type="unfinished">Delete permanently</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="83"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="132"/>
      <location filename="../qml/filemanagement/PQRenamePopout.qml" line="34"/>
      <source>Rename file</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Rename file</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="101"/>
      <source>An error occured, file could not be renamed!</source>
      <translation type="unfinished">An error occured, file could not be renamed!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="112"/>
      <source>Enter new filename</source>
      <translation type="unfinished">Enter new filename</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="82"/>
      <location filename="../qml/filemanagement/PQSaveAsPopout.qml" line="34"/>
      <source>Save file as</source>
      <extracomment>This is a title, similar to all the &apos;save as&apos; options in many programs.
----------
Window title</extracomment>
      <translation type="unfinished">Save file as</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="105"/>
      <source>An error occured, file could not be saved!</source>
      <translation type="unfinished">An error occured, file could not be saved!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="116"/>
      <source>Operation cancelled</source>
      <extracomment>&apos;Operation&apos; here is the operation of saving an image in a new format</extracomment>
      <translation type="unfinished">Operation cancelled</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="124"/>
      <source>Filter formats</source>
      <extracomment>This is a short hint informing the user that here they can &apos;filter all the possible file formats&apos;</extracomment>
      <translation type="unfinished">Filter formats</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="223"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="230"/>
      <source>New filename</source>
      <translation type="unfinished">New filename</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="251"/>
      <source>Choose location and save file</source>
      <translation type="unfinished">Choose location and save file</translation>
    </message>
  </context>
  <context>
    <name>filter</name>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="90"/>
      <source>Filter images in current directory</source>
      <translation type="unfinished">Filter images in current directory</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="102"/>
      <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
      <translation type="unfinished">To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="120"/>
      <source>File name/extension:</source>
      <translation type="unfinished">File name/extension:</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="132"/>
      <source>Enter terms</source>
      <translation type="unfinished">Enter terms</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="161"/>
      <location filename="../qml/filter/PQFilter.qml" line="213"/>
      <source>greater than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
      <translation type="unfinished">greater than</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="163"/>
      <location filename="../qml/filter/PQFilter.qml" line="215"/>
      <source>less than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
      <translation type="unfinished">less than</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="200"/>
      <source>File size</source>
      <translation type="unfinished">File size</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="260"/>
      <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
      <translation type="unfinished">Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="283"/>
      <location filename="../qml/filter/PQFilterPopout.qml" line="34"/>
      <source>Filter</source>
      <extracomment>Written on a clickable button - please keep short
----------
Window title</extracomment>
      <translation type="unfinished">Filter</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="305"/>
      <source>Remove filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation type="unfinished">Remove filter</translation>
    </message>
  </context>
  <context>
    <name>histogram</name>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="143"/>
      <location filename="../qml/histogram/PQHistogramPopout.qml" line="34"/>
      <source>Histogram</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Histogram</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="145"/>
      <source>Loading...</source>
      <extracomment>As in: Loading the histogram for the current image</extracomment>
      <translation type="unfinished">Loading...</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Click-and-drag to move.</source>
      <extracomment>Used for the histogram. The version refers to the type of histogram that is available (colored and greyscale)</extracomment>
      <translation type="unfinished">Click-and-drag to move.</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Right click to switch version.</source>
      <translation type="unfinished">Right click to switch version.</translation>
    </message>
  </context>
  <context>
    <name>imageprovider</name>
    <message>
      <location filename="../cplusplus/imageprovider/imageproviderfull.cpp" line="65"/>
      <location filename="../cplusplus/imageprovider/imageproviderthumb.cpp" line="147"/>
      <source>File failed to load, it does not exist!</source>
      <translation type="unfinished">File failed to load, it does not exist!</translation>
    </message>
  </context>
  <context>
    <name>imgur</name>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="86"/>
      <location filename="../qml/imgur/PQImgurPopout.qml" line="34"/>
      <source>Upload to imgur.com</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Upload to imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="97"/>
      <source>anonymous</source>
      <extracomment>Used as in: Upload image as anonymous user</extracomment>
      <translation type="unfinished">anonymous</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="129"/>
      <source>Obtaining image url...</source>
      <translation type="unfinished">Obtaining image url...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="140"/>
      <source>This seems to take a long time...</source>
      <translation type="unfinished">This seems to take a long time...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="141"/>
      <source>There might be a problem with your internet connection or the imgur.com servers.</source>
      <translation type="unfinished">There might be a problem with your internet connection or the imgur.com servers.</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="152"/>
      <source>An Error occurred while uploading image!</source>
      <translation type="unfinished">An Error occurred while uploading image!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="153"/>
      <source>Error code:</source>
      <translation type="unfinished">Error code:</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="164"/>
      <source>You do not seem to be connected to the internet...</source>
      <translation type="unfinished">You do not seem to be connected to the internet...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="165"/>
      <source>Unable to upload!</source>
      <translation type="unfinished">Unable to upload!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="187"/>
      <source>Access Image</source>
      <translation type="unfinished">Access Image</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="200"/>
      <location filename="../qml/imgur/PQImgur.qml" line="232"/>
      <source>Click to open in browser</source>
      <translation type="unfinished">Click to open in browser</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="207"/>
      <location filename="../qml/imgur/PQImgur.qml" line="239"/>
      <source>Copy to clipboard</source>
      <translation type="unfinished">Copy to clipboard</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="219"/>
      <source>Delete Image</source>
      <translation type="unfinished">Delete Image</translation>
    </message>
  </context>
  <context>
    <name>keymouse</name>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="30"/>
      <source>Alt</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation type="unfinished">Alt</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="32"/>
      <source>Ctrl</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation type="unfinished">Ctrl</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="34"/>
      <source>Shift</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation type="unfinished">Shift</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="36"/>
      <source>Page Up</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Page Up</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="38"/>
      <source>Page Down</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Page Down</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="40"/>
      <source>Meta</source>
      <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
      <translation type="unfinished">Meta</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="42"/>
      <source>Keypad</source>
      <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
      <translation type="unfinished">Keypad</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="44"/>
      <source>Escape</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Escape</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="46"/>
      <source>Right</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation type="unfinished">Right</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="48"/>
      <source>Left</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation type="unfinished">Left</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="50"/>
      <source>Up</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation type="unfinished">Up</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="52"/>
      <source>Down</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation type="unfinished">Down</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="54"/>
      <source>Space</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Space</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="56"/>
      <source>Delete</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Delete</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="58"/>
      <source>Backspace</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Backspace</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="60"/>
      <source>Home</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Home</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="62"/>
      <source>End</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">End</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="64"/>
      <source>Insert</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Insert</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="66"/>
      <source>Tab</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation type="unfinished">Tab</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="69"/>
      <source>Return</source>
      <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
      <translation type="unfinished">Return</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="72"/>
      <source>Enter</source>
      <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
      <translation type="unfinished">Enter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="78"/>
      <source>Left Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Left Button</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="80"/>
      <source>Right Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Right Button</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="82"/>
      <source>Middle Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Middle Button</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="84"/>
      <source>Back Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Back Button</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="86"/>
      <source>Forward Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Forward Button</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="88"/>
      <source>Task Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Task Button</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="90"/>
      <source>Button #7</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Button #7</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="92"/>
      <source>Button #8</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Button #8</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="94"/>
      <source>Button #9</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Button #9</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="96"/>
      <source>Button #10</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation type="unfinished">Button #10</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="98"/>
      <source>Double Click</source>
      <extracomment>Refers to a mouse event</extracomment>
      <translation type="unfinished">Double Click</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="100"/>
      <source>Wheel Up</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation type="unfinished">Wheel Up</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="102"/>
      <source>Wheel Down</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation type="unfinished">Wheel Down</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="104"/>
      <source>East</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation type="unfinished">East</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="106"/>
      <source>South</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation type="unfinished">South</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="108"/>
      <source>West</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation type="unfinished">West</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="110"/>
      <source>North</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation type="unfinished">North</translation>
    </message>
  </context>
  <context>
    <name>logging</name>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="33"/>
      <source>Logging</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Logging</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="109"/>
      <source>enable debug messages</source>
      <translation type="unfinished">enable debug messages</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="134"/>
      <source>copy to clipboard</source>
      <translation type="unfinished">copy to clipboard</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="135"/>
      <source>save to file</source>
      <translation type="unfinished">save to file</translation>
    </message>
  </context>
  <context>
    <name>metadata</name>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="90"/>
      <source>File name</source>
      <extracomment>Please keep string short!</extracomment>
      <translation type="unfinished">File name</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="92"/>
      <source>Dimensions</source>
      <extracomment>The dimensions of the loaded image. Please keep string short!</extracomment>
      <translation type="unfinished">Dimensions</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="96"/>
      <source>File size</source>
      <extracomment>Please keep string short!</extracomment>
      <translation type="unfinished">File size</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="98"/>
      <source>File type</source>
      <extracomment>Please keep string short!</extracomment>
      <translation type="unfinished">File type</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="101"/>
      <source>Make</source>
      <extracomment>Exif image metadata: the make of the camera used to take the photo. Please keep string short!</extracomment>
      <translation type="unfinished">Make</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="103"/>
      <source>Model</source>
      <extracomment>Exif image metadata: the model of the camera used to take the photo. Please keep string short!</extracomment>
      <translation type="unfinished">Model</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="105"/>
      <source>Software</source>
      <extracomment>Exif image metadata: the software used to create the photo. Please keep string short!</extracomment>
      <translation type="unfinished">Software</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="108"/>
      <source>Time Photo was Taken</source>
      <extracomment>Exif image metadata: when the photo was taken. Please keep string short!</extracomment>
      <translation type="unfinished">Time Photo was Taken</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="110"/>
      <source>Exposure Time</source>
      <extracomment>Exif image metadata: how long the sensor was exposed to the light. Please keep string short!</extracomment>
      <translation type="unfinished">Exposure Time</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="112"/>
      <source>Flash</source>
      <extracomment>Exif image metadata: the flash setting when the photo was taken. Please keep string short!</extracomment>
      <translation type="unfinished">Flash</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="115"/>
      <source>Scene Type</source>
      <extracomment>Exif image metadata: the specific scene type the camera used for the photo. Please keep string short!</extracomment>
      <translation type="unfinished">Scene Type</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="117"/>
      <source>Focal Length</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/Focal_length . Please keep string short!</extracomment>
      <translation type="unfinished">Focal Length</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="160"/>
      <location filename="../qml/menumeta/PQMetaDataPopout.qml" line="34"/>
      <source>Metadata</source>
      <extracomment>This is the heading of the metadata element
----------
Window title</extracomment>
      <translation type="unfinished">Metadata</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="199"/>
      <source>No File Loaded</source>
      <translation type="unfinished">No File Loaded</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="252"/>
      <source>Click to open GPS position with online map</source>
      <translation type="unfinished">Click to open GPS position with online map</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="353"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="385"/>
      <source>Click and drag to resize element</source>
      <translation type="unfinished">Click and drag to resize element</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="121"/>
      <source>Light Source</source>
      <extracomment>Exif image metadata: What type of light the camera detected. Please keep string short!</extracomment>
      <translation type="unfinished">Light Source</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="94"/>
      <source>Image</source>
      <extracomment>Used as in &quot;Image 3/16&quot;. The numbers (position of image in folder) are added on automatically. Please keep string short!</extracomment>
      <translation type="unfinished">Image</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="124"/>
      <source>Keywords</source>
      <extracomment>IPTC image metadata: A description of the image by the user/software. Please keep string short!</extracomment>
      <translation type="unfinished">Keywords</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="126"/>
      <source>Location</source>
      <extracomment>IPTC image metadata: The CITY and COUNTRY the imge was taken in. Please keep string short!</extracomment>
      <translation type="unfinished">Location</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="128"/>
      <source>Copyright</source>
      <extracomment>IPTC image metadata. Please keep string short!</extracomment>
      <translation type="unfinished">Copyright</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="131"/>
      <source>GPS Position</source>
      <extracomment>Exif image metadata. Please keep string short!</extracomment>
      <translation type="unfinished">GPS Position</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="119"/>
      <source>F Number</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Please keep string short!</extracomment>
      <translation type="unfinished">F Number</translation>
    </message>
  </context>
  <context>
    <name>navigate</name>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="66"/>
      <source>Click and drag to move</source>
      <translation type="unfinished">Click and drag to move</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="99"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="71"/>
      <source>Navigate to previous image in folder</source>
      <translation type="unfinished">Navigate to previous image in folder</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="128"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="93"/>
      <source>Navigate to next image in folder</source>
      <translation type="unfinished">Navigate to next image in folder</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="154"/>
      <source>Show main menu</source>
      <translation type="unfinished">Show main menu</translation>
    </message>
  </context>
  <context>
    <name>other</name>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="76"/>
      <source>Open a file to start</source>
      <translation type="unfinished">Open a file to start</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="114"/>
      <source>Click anywhere to open a file</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">Click anywhere to open a file</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="124"/>
      <source>Move your cursor to:</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">Move your cursor to:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="134"/>
      <source>RIGHT EDGE for the main menu</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, first option for where to move cursor to</extracomment>
      <translation type="unfinished">RIGHT EDGE for the main menu</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="145"/>
      <source>LEFT EDGE for the metadata</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, second option for where to move cursor to</extracomment>
      <translation type="unfinished">LEFT EDGE for the metadata</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="156"/>
      <source>BOTTOM EDGE to show the thumbnails</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, third option for where to move cursor to</extracomment>
      <translation type="unfinished">BOTTOM EDGE to show the thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="166"/>
      <source>(once an image/folder is loaded)</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation type="unfinished">(once an image/folder is loaded)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="221"/>
      <source>No matches found</source>
      <extracomment>Used as in: No matches found for the currently set filter</extracomment>
      <translation type="unfinished">No matches found</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="233"/>
      <source>PhotoQt Image Viewer</source>
      <extracomment>The window title of PhotoQt</extracomment>
      <translation type="unfinished">PhotoQt Image Viewer</translation>
    </message>
  </context>
  <context>
    <name>popinpopout</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="270"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="295"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="289"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="230"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="230"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="213"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="345"/>
      <location filename="../qml/filter/PQFilter.qml" line="361"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="298"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="425"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="304"/>
      <location filename="../qml/scale/PQScale.qml" line="329"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="371"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="532"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="403"/>
      <source>Merge into main interface</source>
      <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
      <translation type="unfinished">Merge into main interface</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="272"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="297"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="291"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="232"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="232"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="215"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="347"/>
      <location filename="../qml/filter/PQFilter.qml" line="363"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="300"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="427"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="306"/>
      <location filename="../qml/scale/PQScale.qml" line="331"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="373"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="534"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="405"/>
      <source>Move to its own window</source>
      <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
      <translation type="unfinished">Move to its own window</translation>
    </message>
  </context>
  <context>
    <name>quickinfo</name>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="122"/>
      <source>Click here to show main menu</source>
      <translation type="unfinished">Click here to show main menu</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="151"/>
      <source>Click here to enter fullscreen mode</source>
      <translation type="unfinished">Click here to enter fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="152"/>
      <source>Click here to exit fullscreen mode</source>
      <translation type="unfinished">Click here to exit fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="190"/>
      <source>Click here to close PhotoQt</source>
      <translation type="unfinished">Click here to close PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="213"/>
      <source>Show counter</source>
      <translation type="unfinished">Show counter</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="214"/>
      <source>Hide counter</source>
      <translation type="unfinished">Hide counter</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="216"/>
      <source>Show file path</source>
      <translation type="unfinished">Show file path</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="217"/>
      <source>Hide file path</source>
      <translation type="unfinished">Hide file path</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="219"/>
      <source>Show file name</source>
      <translation type="unfinished">Show file name</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="220"/>
      <source>Hide file name</source>
      <translation type="unfinished">Hide file name</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="222"/>
      <source>Show zoom level</source>
      <translation type="unfinished">Show zoom level</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="223"/>
      <source>Hide zoom level</source>
      <translation type="unfinished">Hide zoom level</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="712"/>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="388"/>
      <source>Click here to enter viewer mode</source>
      <translation type="unfinished">Click here to enter viewer mode</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="731"/>
      <source>Hide central &apos;viewer mode&apos; button</source>
      <translation type="unfinished">Hide central &apos;viewer mode&apos; button</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="238"/>
      <source>Some information about the current image and directory</source>
      <translation type="unfinished">Some information about the current image and directory</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="289"/>
      <source>Connected to:</source>
      <extracomment>This is followed by the name of the Chromecast streaming device currently connected to</extracomment>
      <translation type="unfinished">Connected to:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="387"/>
      <source>Click here to exit viewer mode</source>
      <translation type="unfinished">Click here to exit viewer mode</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="342"/>
      <source>Filter:</source>
      <translation type="unfinished">Filter:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="225"/>
      <source>Show window buttons</source>
      <translation type="unfinished">Show window buttons</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="226"/>
      <source>Hide window buttons</source>
      <translation type="unfinished">Hide window buttons</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="329"/>
      <source>Click to remove filter</source>
      <translation type="unfinished">Click to remove filter</translation>
    </message>
  </context>
  <context>
    <name>scale</name>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="84"/>
      <location filename="../qml/scale/PQScalePopout.qml" line="34"/>
      <source>Scale file</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Scale file</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="94"/>
      <source>An error occured, file could not be scaled!</source>
      <translation type="unfinished">An error occured, file could not be scaled!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="104"/>
      <source>This file format cannot (yet) be scaled with PhotoQt!</source>
      <translation type="unfinished">This file format cannot (yet) be scaled with PhotoQt!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="115"/>
      <source>New width x height:</source>
      <translation type="unfinished">New width x height:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="188"/>
      <source>Preserve aspect ratio</source>
      <extracomment>The aspect ratio refers to the ratio of the width to the height of the image, e.g., 16:9 for most movies</extracomment>
      <translation type="unfinished">Preserve aspect ratio</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="199"/>
      <source>Quality:</source>
      <extracomment>This refers to the quality to be used to scale the image</extracomment>
      <translation type="unfinished">Quality:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="232"/>
      <location filename="../qml/scale/PQScale.qml" line="302"/>
      <source>Scale (create new file)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation type="unfinished">Scale (create new file)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="260"/>
      <location filename="../qml/scale/PQScale.qml" line="303"/>
      <source>Scale (change file in place)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation type="unfinished">Scale (change file in place)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="304"/>
      <source>De-/Increase width and height by 10%</source>
      <translation type="unfinished">De-/Increase width and height by 10%</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="305"/>
      <source>In-/Decrease quality by 5%</source>
      <translation type="unfinished">In-/Decrease quality by 5%</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingmanipulation.cpp" line="233"/>
      <source>Select new file</source>
      <translation type="unfinished">Select new file</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager</name>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="100"/>
      <source>interface</source>
      <extracomment>settings manager tab title</extracomment>
      <translation type="unfinished">interface</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="101"/>
      <source>Tab to control interface settings</source>
      <translation type="unfinished">Tab to control interface settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="103"/>
      <source>image view</source>
      <extracomment>settings manager tab title</extracomment>
      <translation type="unfinished">image view</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="104"/>
      <source>Tab to control how images are viewed</source>
      <translation type="unfinished">Tab to control how images are viewed</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="106"/>
      <source>thumbnails</source>
      <extracomment>settings manager tab title</extracomment>
      <translation type="unfinished">thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="107"/>
      <source>Tab to control the look and behaviour of thumbnails</source>
      <translation type="unfinished">Tab to control the look and behaviour of thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="109"/>
      <source>metadata</source>
      <extracomment>settings manager tab title</extracomment>
      <translation type="unfinished">metadata</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="110"/>
      <source>Tab to control metadata settings</source>
      <translation type="unfinished">Tab to control metadata settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="112"/>
      <source>file types</source>
      <extracomment>settings manager tab title</extracomment>
      <translation type="unfinished">file types</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
      <source>Tab to control which file types PhotoQt should recognize</source>
      <translation type="unfinished">Tab to control which file types PhotoQt should recognize</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
      <source>shortcuts</source>
      <extracomment>settings manager tab title</extracomment>
      <translation type="unfinished">shortcuts</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="116"/>
      <source>Tab to control which shortcuts are set</source>
      <translation type="unfinished">Tab to control which shortcuts are set</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="224"/>
      <source>advanced</source>
      <extracomment>Written on button in setting manager. A click on this button opens a menu with some advanced actions.</extracomment>
      <translation type="unfinished">advanced</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="228"/>
      <source>restore defaults</source>
      <translation type="unfinished">restore defaults</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="229"/>
      <source>import settings</source>
      <translation type="unfinished">import settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="230"/>
      <source>export settings</source>
      <translation type="unfinished">export settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>disable expert mode</source>
      <translation type="unfinished">disable expert mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>enable expert mode</source>
      <translation type="unfinished">enable expert mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="272"/>
      <source>Import of %1. This will replace your current settings with the ones stored in the backup.</source>
      <translation type="unfinished">Import of %1. This will replace your current settings with the ones stored in the backup.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="273"/>
      <source>Do you want to continue?</source>
      <translation type="unfinished">Do you want to continue?</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="325"/>
      <source>Save changes and exit</source>
      <translation type="unfinished">Save changes and exit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="338"/>
      <source>Exit and discard changes</source>
      <translation type="unfinished">Exit and discard changes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="179"/>
      <source>Rename File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Rename File</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="181"/>
      <source>Delete File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Delete File</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="76"/>
      <source>Filetype settings</source>
      <translation type="unfinished">Filetype settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>These settings govern which file types PhotoQt should recognize and open.</source>
      <translation type="unfinished">These settings govern which file types PhotoQt should recognize and open.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>Not all file types might be available, depending on your setup and what library support was enabled at compile time</source>
      <translation type="unfinished">Not all file types might be available, depending on your setup and what library support was enabled at compile time</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="84"/>
      <source>Image view settings</source>
      <translation type="unfinished">Image view settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <source>These settings affect the viewing of images, how they are shown, in what order, how large a cache to use, etc.</source>
      <translation type="unfinished">These settings affect the viewing of images, how they are shown, in what order, how large a cache to use, etc.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>Some settings are only shown in expert mode.</source>
      <translation type="unfinished">Some settings are only shown in expert mode.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="86"/>
      <source>Interface settings</source>
      <translation type="unfinished">Interface settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <source>These settings affect the interface in general, how the application looks like and behaves. This includes the background, some of the labels in the main view, which elements are to be shown in their own window, and others.</source>
      <translation type="unfinished">These settings affect the interface in general, how the application looks like and behaves. This includes the background, some of the labels in the main view, which elements are to be shown in their own window, and others.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="85"/>
      <source>Metadata settings</source>
      <translation type="unfinished">Metadata settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <source>These settings affect the metadata element, what information it should show and some of its behavior.</source>
      <translation type="unfinished">These settings affect the metadata element, what information it should show and some of its behavior.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="77"/>
      <source>Shortcuts</source>
      <translation type="unfinished">Shortcuts</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="101"/>
      <source>Navigation</source>
      <extracomment>A shortcuts category: navigation</extracomment>
      <translation type="unfinished">Navigation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="107"/>
      <source>Filter images in folder</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Filter images in folder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="109"/>
      <source>Next image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Next image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="111"/>
      <source>Previous image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Previous image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="113"/>
      <source>Load a random image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Load a random image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="117"/>
      <source>Go to first image</source>
      <extracomment>Name of shortcut action Name of shortcut action</extracomment>
      <translation type="unfinished">Go to first image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="119"/>
      <source>Go to last image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Go to last image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="121"/>
      <source>Enter viewer mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Enter viewer mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="125"/>
      <source>Close window (hides to system tray if enabled)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Close window (hides to system tray if enabled)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="127"/>
      <source>Quit PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Quit PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="135"/>
      <source>Image</source>
      <extracomment>A shortcuts category: image manipulation</extracomment>
      <translation type="unfinished">Image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="139"/>
      <source>Zoom In</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Zoom In</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="141"/>
      <source>Zoom Out</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Zoom Out</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="143"/>
      <source>Zoom to Actual Size</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Zoom to Actual Size</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="145"/>
      <source>Reset Zoom</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Reset Zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="147"/>
      <source>Toggle &apos;Fit in window&apos;</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Toggle &apos;Fit in window&apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="149"/>
      <source>Rotate Right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Rotate Right</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="151"/>
      <source>Rotate Left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Rotate Left</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="153"/>
      <source>Reset Rotation</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Reset Rotation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="155"/>
      <source>Flip Horizontally</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Flip Horizontally</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="157"/>
      <source>Flip Vertically</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Flip Vertically</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="159"/>
      <source>Scale Image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Scale Image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="161"/>
      <source>Play/Pause animation/video</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Play/Pause animation/video</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="163"/>
      <source>Hide/Show face tags (stored in metadata)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show face tags (stored in metadata)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="167"/>
      <source>Advanced image sort (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Advanced image sort (Quickstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="215"/>
      <source>Start Slideshow (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Start Slideshow (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="245"/>
      <source>filename including path</source>
      <translation type="unfinished">filename including path</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="246"/>
      <source>filename without path</source>
      <translation type="unfinished">filename without path</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="247"/>
      <source>directory containing file</source>
      <translation type="unfinished">directory containing file</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="175"/>
      <source>File</source>
      <extracomment>A shortcuts category: file management</extracomment>
      <translation type="unfinished">File</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="123"/>
      <source>Show floating navigation buttons</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show floating navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="183"/>
      <source>Delete File (without confirmation)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Delete File (without confirmation)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="185"/>
      <source>Copy File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Copy File to a New Location</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="187"/>
      <source>Move File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Move File to a New Location</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="189"/>
      <source>Copy Image to Clipboard</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Copy Image to Clipboard</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="193"/>
      <source>Print current photo</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Print current photo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="201"/>
      <source>Other</source>
      <extracomment>A shortcuts category: other functions</extracomment>
      <translation type="unfinished">Other</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="205"/>
      <source>Hide/Show main menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show main menu</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="207"/>
      <source>Hide/Show metadata</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show metadata</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="209"/>
      <source>Keep metadata opened</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Keep metadata opened</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="211"/>
      <source>Hide/Show thumbnails</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Hide/Show thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="213"/>
      <source>Show Settings</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show Settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="217"/>
      <source>Start Slideshow (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Start Slideshow (Quickstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="219"/>
      <source>About PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">About PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="221"/>
      <source>Set as Wallpaper</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Set as Wallpaper</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="223"/>
      <source>Show Histogram</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show Histogram</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="225"/>
      <source>Upload to imgur.com (anonymously)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Upload to imgur.com (anonymously)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="227"/>
      <source>Upload to imgur.com user account</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Upload to imgur.com user account</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="229"/>
      <source>Stream content to Chromecast device</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Stream content to Chromecast device</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="231"/>
      <source>Show log/debug messages</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Show log/debug messages</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="233"/>
      <source>Toggle fullscreen mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Toggle fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="243"/>
      <source>External</source>
      <extracomment>A shortcuts category: external shortcuts</extracomment>
      <translation type="unfinished">External</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="85"/>
      <source>Thumbnails settings</source>
      <translation type="unfinished">Thumbnails settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>These settings affect the thumbnails shown, by default, along the bottom edge of the screen. This includes their look, behavior, and the user&apos;s interaction with them.</source>
      <translation type="unfinished">These settings affect the thumbnails shown, by default, along the bottom edge of the screen. This includes their look, behavior, and the user&apos;s interaction with them.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManagerPopout.qml" line="34"/>
      <source>Settings Manager</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Settings Manager</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>Here the shortcuts can be managed. Below you can add a new shortcut for any one of the available actions, both key combinations and mouse gestures are supported.</source>
      <translation type="unfinished">Here the shortcuts can be managed. Below you can add a new shortcut for any one of the available actions, both key combinations and mouse gestures are supported.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>You can also set the same shortcut for multiple actions or multiple times for the same action. All actions for a shortcut will be executed sequentially, allowing a lot more flexibility in using PhotoQt.</source>
      <translation type="unfinished">You can also set the same shortcut for multiple actions or multiple times for the same action. All actions for a shortcut will be executed sequentially, allowing a lot more flexibility in using PhotoQt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="105"/>
      <source>Open file (browse images)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Open file (browse images)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="165"/>
      <source>Advanced image sort (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Advanced image sort (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="191"/>
      <source>Save image in another format</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation type="unfinished">Save image in another format</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="72"/>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="128"/>
      <source>Restore defaults</source>
      <extracomment>As in &apos;restore the default settings and/or file formats and/or shortcuts&apos;. Please keep short!</extracomment>
      <translation type="unfinished">Restore defaults</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="83"/>
      <source>Here you can restore the default configuration of PhotoQt. You can choose to restore any combination of the following three categories.</source>
      <translation type="unfinished">Here you can restore the default configuration of PhotoQt. You can choose to restore any combination of the following three categories.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="96"/>
      <source>Restore default settings</source>
      <translation type="unfinished">Restore default settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="103"/>
      <source>Restore default file formats</source>
      <translation type="unfinished">Restore default file formats</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="109"/>
      <source>Restore default shortcuts</source>
      <translation type="unfinished">Restore default shortcuts</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_filetypes</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="32"/>
      <source>These are some additional settings for opening archives.</source>
      <translation type="unfinished">These are some additional settings for opening archives.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="45"/>
      <source>use external tool: unrar</source>
      <extracomment>used as label for checkbox</extracomment>
      <translation type="unfinished">use external tool: unrar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="32"/>
      <source>These are some additional settings for showing PDFs.</source>
      <translation type="unfinished">These are some additional settings for showing PDFs.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="44"/>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="55"/>
      <source>Quality:</source>
      <extracomment>the quality setting to be used when loading PDFs</extracomment>
      <translation type="unfinished">Quality:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="32"/>
      <source>These are some additional settings for playing videos.</source>
      <translation type="unfinished">These are some additional settings for playing videos.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="45"/>
      <source>Autoplay</source>
      <extracomment>Used as setting for video files (i.e., autoplay videos)</extracomment>
      <translation type="unfinished">Autoplay</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="51"/>
      <source>Loop</source>
      <extracomment>Used as setting for video files (i.e., loop videos)</extracomment>
      <translation type="unfinished">Loop</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="58"/>
      <source>Prefer libmpv</source>
      <extracomment>Used as setting for video files</extracomment>
      <translation type="unfinished">Prefer libmpv</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="59"/>
      <source>libmpv can offer a more comprehensive video support but tends to be slightly slower to load.</source>
      <translation type="unfinished">libmpv can offer a more comprehensive video support but tends to be slightly slower to load.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="64"/>
      <source>Select tool for creating video thumbnails</source>
      <extracomment>Tooltip shown for combobox for selectiong video thumbnailer</extracomment>
      <translation type="unfinished">Select tool for creating video thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="55"/>
      <source>images</source>
      <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
      <translation type="unfinished">images</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="57"/>
      <source>compressed files</source>
      <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
      <translation type="unfinished">compressed files</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="59"/>
      <source>documents</source>
      <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
      <translation type="unfinished">documents</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="61"/>
      <source>videos</source>
      <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
      <translation type="unfinished">videos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="67"/>
      <source>Enable</source>
      <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
      <translation type="unfinished">Enable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="73"/>
      <source>Disable</source>
      <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
      <translation type="unfinished">Disable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="99"/>
      <source>Enable everything</source>
      <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
      <translation type="unfinished">Enable everything</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="117"/>
      <source>Currently there are %1 file formats enabled</source>
      <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
      <translation type="unfinished">Currently there are %1 file formats enabled</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="145"/>
      <source>Search by description or file ending</source>
      <translation type="unfinished">Search by description or file ending</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="151"/>
      <source>Search by image library or category</source>
      <translation type="unfinished">Search by image library or category</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="240"/>
      <source>File endings:</source>
      <translation type="unfinished">File endings:</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_imageview</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="31"/>
      <source>animation</source>
      <extracomment>A settings title referring to the in/out animation of images</extracomment>
      <translation type="unfinished">animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="33"/>
      <source>What type of animation to show, and how fast.</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">What type of animation to show, and how fast.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="44"/>
      <source>type of animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">type of animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="47"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">opacity</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="49"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">along x-axis</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="51"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">along y-axis</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="53"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">rotation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="55"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">explosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="57"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">implosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="59"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">choose one at random</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="76"/>
      <source>no animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">no animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="91"/>
      <source>long animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">long animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="30"/>
      <source>fit in window</source>
      <extracomment>A settings title referring to whether to fit images in window</extracomment>
      <translation type="unfinished">fit in window</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="31"/>
      <source>Zoom smaller images to fill the full window width and/or height.</source>
      <translation type="unfinished">Zoom smaller images to fill the full window width and/or height.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="36"/>
      <source>fit smaller images in window</source>
      <translation type="unfinished">fit smaller images in window</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="30"/>
      <source>interpolation</source>
      <extracomment>A settings title referring to the type of interpolation to use for small images</extracomment>
      <translation type="unfinished">interpolation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="31"/>
      <source>PhotoQt tries to improve the rendering of images that are shown much larger than they are (i.e., zoomed in a lot). For very tiny images that are zoomed in quite a lot, this can result in the loss of too much information in the image. Thus a threshold can be defined here, images that are smaller than this threshold are shown exactly as they are without any smoothing or other attempts to improve them.</source>
      <translation type="unfinished">PhotoQt tries to improve the rendering of images that are shown much larger than they are (i.e., zoomed in a lot). For very tiny images that are zoomed in quite a lot, this can result in the loss of too much information in the image. Thus a threshold can be defined here, images that are smaller than this threshold are shown exactly as they are without any smoothing or other attempts to improve them.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="42"/>
      <source>Do not use any interpolation algorithm for very small images</source>
      <extracomment>A type of interpolation to use for small images</extracomment>
      <translation type="unfinished">Do not use any interpolation algorithm for very small images</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="53"/>
      <source>threshold:</source>
      <extracomment>The threshold (in pixels) at which to switch interpolation algorithm</extracomment>
      <translation type="unfinished">threshold:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="30"/>
      <source>remember per session</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">remember per session</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="31"/>
      <source>By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</source>
      <translation type="unfinished">By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="37"/>
      <source>remember zoom, rotation, flip, position</source>
      <translation type="unfinished">remember zoom, rotation, flip, position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="30"/>
      <source>looping</source>
      <extracomment>A settings title for looping through images in folder</extracomment>
      <translation type="unfinished">looping</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="31"/>
      <source>What to do when the end of a folder has been reached: stop or loop back to first image in folder.</source>
      <translation type="unfinished">What to do when the end of a folder has been reached: stop or loop back to first image in folder.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="36"/>
      <source>loop through images in folder</source>
      <translation type="unfinished">loop through images in folder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="30"/>
      <source>margin</source>
      <extracomment>A settings title about the margin around the main image</extracomment>
      <translation type="unfinished">margin</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="31"/>
      <source>How much space to show between the main image and the application border.</source>
      <translation type="unfinished">How much space to show between the main image and the application border.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="44"/>
      <source>none</source>
      <extracomment>As in: no margin between the main image and the window edges</extracomment>
      <translation type="unfinished">none</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="30"/>
      <source>pixmap cache</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">pixmap cache</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="31"/>
      <source>Size of runtime cache for fully loaded images. This cache is cleared when the application quits.</source>
      <translation type="unfinished">Size of runtime cache for fully loaded images. This cache is cleared when the application quits.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="31"/>
      <source>sort images by</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">sort images by</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="32"/>
      <source>Sort all images in a folder by the set property.</source>
      <translation type="unfinished">Sort all images in a folder by the set property.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="43"/>
      <source>natural name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation type="unfinished">natural name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="45"/>
      <source>name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation type="unfinished">name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="47"/>
      <source>time</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation type="unfinished">time</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="49"/>
      <source>size</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation type="unfinished">size</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="51"/>
      <source>type</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation type="unfinished">type</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="58"/>
      <source>ascending</source>
      <extracomment>Sort images in ascending order</extracomment>
      <translation type="unfinished">ascending</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="65"/>
      <source>descending</source>
      <extracomment>Sort images in descending order</extracomment>
      <translation type="unfinished">descending</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="30"/>
      <source>transparency marker</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">transparency marker</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="31"/>
      <source>Show checkerboard pattern behind transparent areas of (half-)transparent images.</source>
      <translation type="unfinished">Show checkerboard pattern behind transparent areas of (half-)transparent images.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="37"/>
      <source>show checkerboard pattern</source>
      <extracomment>Setting for how to display images that have transparent areas, whether to show checkerboard pattern in that area or not</extracomment>
      <translation type="unfinished">show checkerboard pattern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="30"/>
      <source>zoom speed</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">zoom speed</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="31"/>
      <source>Images are zoomed at a relative speed as specified by this percentage. A higher value means faster zoom.</source>
      <translation type="unfinished">Images are zoomed at a relative speed as specified by this percentage. A higher value means faster zoom.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="43"/>
      <source>super slow</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">super slow</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="58"/>
      <source>very fast</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">very fast</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="31"/>
      <source>zoom to/from</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">zoom to/from</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="32"/>
      <source>This controls whether the image is zoomed to/from the mouse position or to/from the image center. Note that this only applies when zooming by mouse, zooming by keyboard shortcut always zooms to/from the image center.</source>
      <translation type="unfinished">This controls whether the image is zoomed to/from the mouse position or to/from the image center. Note that this only applies when zooming by mouse, zooming by keyboard shortcut always zooms to/from the image center.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="42"/>
      <source>mouse position</source>
      <translation type="unfinished">mouse position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="47"/>
      <source>image center</source>
      <translation type="unfinished">image center</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="30"/>
      <source>zoom min/max</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation type="unfinished">zoom min/max</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>This defines the minimum and/or maximum zoom level for an image.</source>
      <translation type="unfinished">This defines the minimum and/or maximum zoom level for an image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
      <translation type="unfinished">Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="52"/>
      <source>minimum zoom:</source>
      <translation type="unfinished">minimum zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="91"/>
      <source>maximum zoom:</source>
      <translation type="unfinished">maximum zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="30"/>
      <source>Double Click Threshold</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">Double Click Threshold</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="31"/>
      <source>Two clicks within the specified interval are interpreted as a double click. Note that too high a value will cause noticable delays in reacting to single clicks.</source>
      <translation type="unfinished">Two clicks within the specified interval are interpreted as a double click. Note that too high a value will cause noticable delays in reacting to single clicks.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_interface</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="32"/>
      <source>background</source>
      <extracomment>A settings title referring to the background of PhotoQt (behind any image/element)</extracomment>
      <translation type="unfinished">background</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="34"/>
      <source>What type of background is to be shown.</source>
      <extracomment>The background here refers to the area behind the main image and any element in PhotoQt, the very back.</extracomment>
      <translation type="unfinished">What type of background is to be shown.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="45"/>
      <source>(half-)transparent background</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation type="unfinished">(half-)transparent background</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="47"/>
      <source>faked transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation type="unfinished">faked transparency</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="49"/>
      <source>custom background image</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation type="unfinished">custom background image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="76"/>
      <source>Click to select an image</source>
      <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
      <translation type="unfinished">Click to select an image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="87"/>
      <source>scale to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation type="unfinished">scale to fit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="89"/>
      <source>scale and crop to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation type="unfinished">scale and crop to fit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="91"/>
      <source>stretch to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation type="unfinished">stretch to fit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="93"/>
      <source>center image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation type="unfinished">center image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="95"/>
      <source>tile image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation type="unfinished">tile image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="201"/>
      <source>All Images</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: All images supported by PhotoQt.</extracomment>
      <translation type="unfinished">All Images</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="216"/>
      <source>Video</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: Video files supported by PhotoQt.</extracomment>
      <translation type="unfinished">Video</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="31"/>
      <source>empty area around image</source>
      <extracomment>A settings title</extracomment>
      <translation type="unfinished">empty area around image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="32"/>
      <source>How to handle clicks on empty area around images.</source>
      <translation type="unfinished">How to handle clicks on empty area around images.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="40"/>
      <source>close on click</source>
      <extracomment>Used as in: Close PhotoQt on click on empty area around main image</extracomment>
      <translation type="unfinished">close on click</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="41"/>
      <source>Close PhotoQt when click occurred on empty area around image</source>
      <translation type="unfinished">Close PhotoQt when click occurred on empty area around image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="52"/>
      <source>navigate on click</source>
      <extracomment>Used as in: Navigate in folder on click on empty area around main image</extracomment>
      <translation type="unfinished">navigate on click</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="53"/>
      <source>Go to next/previous image if click occurred in left/right half of window</source>
      <translation type="unfinished">Go to next/previous image if click occurred in left/right half of window</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="63"/>
      <source>toggle window decoration</source>
      <translation type="unfinished">toggle window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="64"/>
      <source>Toggle window decoration</source>
      <translation type="unfinished">Toggle window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="30"/>
      <source>size of &apos;hot edge&apos;</source>
      <extracomment>A settings title. The hot edge refers to the area along the edges of PhotoQt where the mouse cursor triggers an action (e.g., showing the thumbnails or the main menu)</extracomment>
      <translation type="unfinished">size of &apos;hot edge&apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="31"/>
      <source>Adjusts the sensitivity of the edges for showing elements like the metadata and main menu elements.</source>
      <translation type="unfinished">Adjusts the sensitivity of the edges for showing elements like the metadata and main menu elements.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="40"/>
      <source>small</source>
      <extracomment>used as in: small area</extracomment>
      <translation type="unfinished">small</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="57"/>
      <source>large</source>
      <extracomment>used as in: large area</extracomment>
      <translation type="unfinished">large</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="33"/>
      <source>language</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">language</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="34"/>
      <source>Change the language of the application.</source>
      <translation type="unfinished">Change the language of the application.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="30"/>
      <source>mouse wheel sensitivity</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">mouse wheel sensitivity</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="31"/>
      <source>How sensitive the mouse wheel is for shortcuts, etc.</source>
      <translation type="unfinished">How sensitive the mouse wheel is for shortcuts, etc.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="58"/>
      <source>not sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation type="unfinished">not sensitive</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="42"/>
      <source>very sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation type="unfinished">very sensitive</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="32"/>
      <source>overlay color</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">overlay color</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="33"/>
      <source>This is the color that is shown in the background on top of any background image/etc.</source>
      <translation type="unfinished">This is the color that is shown in the background on top of any background image/etc.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="77"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="148"/>
      <source>click to change color</source>
      <translation type="unfinished">click to change color</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="122"/>
      <source>fullscreen mode</source>
      <translation type="unfinished">fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="186"/>
      <source>use same color in window and fullscreen mode</source>
      <translation type="unfinished">use same color in window and fullscreen mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="195"/>
      <source>please choose a color</source>
      <translation type="unfinished">please choose a color</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="31"/>
      <source>pop out elements</source>
      <extracomment>A settings title. The popping out that is talked about here refers to the possibility of showing any element in its own window (i.e., popped out).</extracomment>
      <translation type="unfinished">pop out elements</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="32"/>
      <source>Here you can choose for most elements whether they are to be shown integrated into the main window or in their own, separate window.</source>
      <translation type="unfinished">Here you can choose for most elements whether they are to be shown integrated into the main window or in their own, separate window.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>File dialog</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">File dialog</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>keep open</source>
      <translation type="unfinished">keep open</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="37"/>
      <source>Settings Manager</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Settings Manager</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="39"/>
      <source>Main Menu</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Main Menu</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="41"/>
      <source>Metadata</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Metadata</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="43"/>
      <source>Histogram</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Histogram</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="45"/>
      <source>Scale</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Scale</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="47"/>
      <source>Slideshow Settings</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Slideshow Settings</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="49"/>
      <source>Slideshow Controls</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Slideshow Controls</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="51"/>
      <source>Rename File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Rename File</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="53"/>
      <source>Delete File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Delete File</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="55"/>
      <source>Save File As</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Save File As</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="57"/>
      <source>About</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">About</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="59"/>
      <source>Imgur</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Imgur</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="61"/>
      <source>Wallpaper</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Wallpaper</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="63"/>
      <source>Filter</source>
      <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Filter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="65"/>
      <source>Advanced Image Sort</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation type="unfinished">Advanced Image Sort</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="31"/>
      <source>status information</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">status information</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="32"/>
      <source>The status information shows some basic data about the current folder and photo in the top left corner of the window. The items can be reordered using drag-and-drop.</source>
      <translation type="unfinished">The status information shows some basic data about the current folder and photo in the top left corner of the window. The items can be reordered using drag-and-drop.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="41"/>
      <source>Show status information</source>
      <translation type="unfinished">Show status information</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="72"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="204"/>
      <source>counter</source>
      <extracomment>Please keep short! The counter shows where we are in the folder.</extracomment>
      <translation type="unfinished">counter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="76"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="208"/>
      <source>filepath</source>
      <extracomment>Please keep short!</extracomment>
      <translation type="unfinished">filepath</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="78"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="210"/>
      <source>resolution</source>
      <extracomment>Please keep short! This is the image resolution.</extracomment>
      <translation type="unfinished">resolution</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="80"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="212"/>
      <source>zoom</source>
      <extracomment>Please keep short! This is the current zoom level.</extracomment>
      <translation type="unfinished">zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="82"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="214"/>
      <source>rotation</source>
      <extracomment>Please keep short! This is the rotation of the current image</extracomment>
      <translation type="unfinished">rotation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="84"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="216"/>
      <source>filesize</source>
      <extracomment>Please keep short! This is the filesize of the current image.</extracomment>
      <translation type="unfinished">filesize</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="231"/>
      <source>Add</source>
      <extracomment>This is written on a button that is used to add a selected block to the status info section.</extracomment>
      <translation type="unfinished">Add</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="242"/>
      <source>Font size:</source>
      <translation type="unfinished">Font size:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="74"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="206"/>
      <source>filename</source>
      <extracomment>Please keep short!</extracomment>
      <translation type="unfinished">filename</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="31"/>
      <source>window buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="32"/>
      <source>PhotoQt can show some integrated window buttons at the top right corner of the screen.</source>
      <translation type="unfinished">PhotoQt can show some integrated window buttons at the top right corner of the screen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="44"/>
      <source>show integrated window buttons</source>
      <translation type="unfinished">show integrated window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="50"/>
      <source>always show &apos;x&apos;</source>
      <translation type="unfinished">always show &apos;x&apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="61"/>
      <source>size of window buttons</source>
      <extracomment>the size of the window buttons (the buttons shown in the top right corner of the window)</extracomment>
      <translation type="unfinished">size of window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="30"/>
      <source>remember last image</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">remember last image</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="31"/>
      <source>At startup the image loaded at the end of the last session can be automatically reloaded.</source>
      <translation type="unfinished">At startup the image loaded at the end of the last session can be automatically reloaded.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="35"/>
      <source>re-open last loaded image at startup</source>
      <translation type="unfinished">re-open last loaded image at startup</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="30"/>
      <source>tray icon</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">tray icon</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="31"/>
      <source>If a tray icon is to be shown and, if shown, whether to hide it or not.</source>
      <translation type="unfinished">If a tray icon is to be shown and, if shown, whether to hide it or not.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="36"/>
      <source>no tray icon</source>
      <translation type="unfinished">no tray icon</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="37"/>
      <source>hide to tray icon</source>
      <translation type="unfinished">hide to tray icon</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="38"/>
      <source>show tray icon but don&apos;t hide to it</source>
      <translation type="unfinished">show tray icon but don&apos;t hide to it</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="31"/>
      <source>window management</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">window management</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="32"/>
      <source>Some basic window management properties.</source>
      <translation type="unfinished">Some basic window management properties.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="40"/>
      <source>manage window through quick info labels</source>
      <translation type="unfinished">manage window through quick info labels</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="46"/>
      <source>save and restore window geometry</source>
      <translation type="unfinished">save and restore window geometry</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="51"/>
      <source>keep above other windows</source>
      <translation type="unfinished">keep above other windows</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="54"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="31"/>
      <source>window mode</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">window mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="32"/>
      <source>Whether to run PhotoQt in window mode or fullscreen.</source>
      <translation type="unfinished">Whether to run PhotoQt in window mode or fullscreen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="40"/>
      <source>run in window mode</source>
      <translation type="unfinished">run in window mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="45"/>
      <source>show window decoration</source>
      <translation type="unfinished">show window decoration</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="34"/>
      <source>custom context menu entries</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">custom context menu entries</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="35"/>
      <source>Add some custom entries to the context menu.</source>
      <translation type="unfinished">Add some custom entries to the context menu.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="58"/>
      <source>Set entries for other image related applications</source>
      <translation type="unfinished">Set entries for other image related applications</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="60"/>
      <source>This will look for some other image related applications on your computer and add an entry for any that are found.</source>
      <translation type="unfinished">This will look for some other image related applications on your computer and add an entry for any that are found.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="61"/>
      <source>Note that this will replace all entries currently set and cannot be undone.</source>
      <translation type="unfinished">Note that this will replace all entries currently set and cannot be undone.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="111"/>
      <source>Click to change the icon of the entry</source>
      <translation type="unfinished">Click to change the icon of the entry</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="128"/>
      <source>what string to show for this entry</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation type="unfinished">what string to show for this entry</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="150"/>
      <source>Click here to select an executable to be used with this shortcut.</source>
      <translation type="unfinished">Click here to select an executable to be used with this shortcut.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="153"/>
      <source>executable</source>
      <extracomment>This is written on a button, used as in &apos;click this button to select an executable&apos;</extracomment>
      <translation type="unfinished">executable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="174"/>
      <source>additional command line flags</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation type="unfinished">additional command line flags</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="198"/>
      <source>quit</source>
      <extracomment>Keep string short! Used on checkbox for contextmenu, refers to option to close PhotoQt after respective command has been executed.</extracomment>
      <translation type="unfinished">quit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="218"/>
      <source>move entry down</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry down in the list of all entries&apos;</extracomment>
      <translation type="unfinished">move entry down</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="231"/>
      <source>move entry up</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry up in the list of all entries&apos;</extracomment>
      <translation type="unfinished">move entry up</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="249"/>
      <source>delete entry</source>
      <extracomment>contextmenu settings: used as in &apos;delete this entry out of the list of all entries&apos;</extracomment>
      <translation type="unfinished">delete entry</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="277"/>
      <source>Also show entries in main menu</source>
      <translation type="unfinished">Also show entries in main menu</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="31"/>
      <source>navigation buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">navigation buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="32"/>
      <source>Some buttons to help with navigation. These can come in handy when, e.g., operating with a touch screen.</source>
      <translation type="unfinished">Some buttons to help with navigation. These can come in handy when, e.g., operating with a touch screen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="41"/>
      <source>buttons next to window buttons</source>
      <translation type="unfinished">buttons next to window buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="46"/>
      <source>floating buttons</source>
      <translation type="unfinished">floating buttons</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="30"/>
      <source>popout when needed</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">popout when needed</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="31"/>
      <source>Some elements require a minimum amount of space. When the window is smaller than that, those elements can be automatically popped out to provide the space they need.</source>
      <translation type="unfinished">Some elements require a minimum amount of space. When the window is smaller than that, those elements can be automatically popped out to provide the space they need.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="36"/>
      <source>pop out when application window is small</source>
      <translation type="unfinished">pop out when application window is small</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_metadata</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="31"/>
      <source>face tags</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation type="unfinished">face tags</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="33"/>
      <source>Whether to show face tags (stored in metadata info).</source>
      <extracomment>The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation type="unfinished">Whether to show face tags (stored in metadata info).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="38"/>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="37"/>
      <source>enable</source>
      <translation type="unfinished">enable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="32"/>
      <source>face tags - border</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation type="unfinished">face tags - border</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="33"/>
      <source>If and what style of border to show around tagged faces.</source>
      <translation type="unfinished">If and what style of border to show around tagged faces.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="45"/>
      <source>show border</source>
      <extracomment>The border here is the border around face tags.</extracomment>
      <translation type="unfinished">show border</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="96"/>
      <source>click to change color</source>
      <translation type="unfinished">click to change color</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="110"/>
      <source>please choose a color</source>
      <translation type="unfinished">please choose a color</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="31"/>
      <source>face tags - font size</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation type="unfinished">face tags - font size</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="33"/>
      <source>The font size of the name labels.</source>
      <extracomment>The name labels here are the labels with the name used for the face tags.</extracomment>
      <translation type="unfinished">The font size of the name labels.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="31"/>
      <source>face tags - visibility</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation type="unfinished">face tags - visibility</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="32"/>
      <source>When to show the face tags and for how long.</source>
      <translation type="unfinished">When to show the face tags and for how long.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="38"/>
      <source>hybrid mode</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation type="unfinished">hybrid mode</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="40"/>
      <source>always show all</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation type="unfinished">always show all</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="42"/>
      <source>show one on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation type="unfinished">show one on hover</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="44"/>
      <source>show all on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation type="unfinished">show all on hover</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="31"/>
      <source>GPS online map</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">GPS online map</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="32"/>
      <source>Which map service to use when a GPS position is clicked.</source>
      <translation type="unfinished">Which map service to use when a GPS position is clicked.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="31"/>
      <source>meta information</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">meta information</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="32"/>
      <source>Which meta information to extract and display.</source>
      <translation type="unfinished">Which meta information to extract and display.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="35"/>
      <source>file name</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">file name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="37"/>
      <source>file type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">file type</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="39"/>
      <source>file size</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">file size</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="41"/>
      <source>image #/#</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">image #/#</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="43"/>
      <source>dimensions</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">dimensions</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="45"/>
      <source>copyright</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">copyright</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="47"/>
      <source>exposure time</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">exposure time</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="49"/>
      <source>flash</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">flash</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="51"/>
      <source>focal length</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">focal length</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="53"/>
      <source>f-number</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">f-number</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="55"/>
      <source>GPS position</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">GPS position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="58"/>
      <source>keywords</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">keywords</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="60"/>
      <source>light source</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">light source</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="62"/>
      <source>location</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">location</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="64"/>
      <source>make</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">make</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="66"/>
      <source>model</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">model</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="68"/>
      <source>scene type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">scene type</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="70"/>
      <source>software</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">software</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="72"/>
      <source>time photo was taken</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation type="unfinished">time photo was taken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="30"/>
      <source>auto-rotation</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">auto-rotation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="31"/>
      <source>Automatically rotate images based on metadata information.</source>
      <translation type="unfinished">Automatically rotate images based on metadata information.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQBehindLeftEdge.qml" line="31"/>
      <source>how to access</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">how to access</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQBehindLeftEdge.qml" line="32"/>
      <source>The meta data can be accessed in two different ways: 1) A floating element that can be moved freely, or 2) hidden behind the left screen edge and shown when the mouse cursor is moved there.</source>
      <translation type="unfinished">The meta data can be accessed in two different ways: 1) A floating element that can be moved freely, or 2) hidden behind the left screen edge and shown when the mouse cursor is moved there.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_shortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="61"/>
      <source>quit</source>
      <extracomment>checkbox in shortcuts settings, used as in: quit PhotoQt. Please keep as short as possible!</extracomment>
      <translation type="unfinished">quit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="96"/>
      <source>Click to change shortcut</source>
      <translation type="unfinished">Click to change shortcut</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="130"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="144"/>
      <source>Click to delete shortcut</source>
      <translation type="unfinished">Click to delete shortcut</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="89"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="61"/>
      <source>no shortcut set</source>
      <translation type="unfinished">no shortcut set</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="71"/>
      <source>Click to manage shortcut</source>
      <translation type="unfinished">Click to manage shortcut</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="172"/>
      <source>add new</source>
      <extracomment>Used as in &apos;add new shortcut&apos;. Please keep short!</extracomment>
      <translation type="unfinished">add new</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="113"/>
      <source>Add New Shortcut</source>
      <translation type="unfinished">Add New Shortcut</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="134"/>
      <source>Perform a mouse gesture here or press any key combo</source>
      <translation type="unfinished">Perform a mouse gesture here or press any key combo</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalContainer.qml" line="47"/>
      <source>Add new</source>
      <extracomment>Used on button as in &apos;add new external shortcut&apos;. Please keep short!</extracomment>
      <translation type="unfinished">Add new</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_thumbnails</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="30"/>
      <source>thumbnail cache</source>
      <extracomment>A settings title.</extracomment>
      <translation type="unfinished">thumbnail cache</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="31"/>
      <source>Thumbnails can be cached (permanently), following the freedesktop.org standard.</source>
      <translation type="unfinished">Thumbnails can be cached (permanently), following the freedesktop.org standard.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="36"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="45"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="46"/>
      <source>enable</source>
      <translation type="unfinished">enable</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="30"/>
      <source>keep in center</source>
      <extracomment>A settings title. Used as in: Keep thumbnail for current main image in center.</extracomment>
      <translation type="unfinished">keep in center</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="31"/>
      <source>Keep currently active thumbnail in the center of the screen</source>
      <translation type="unfinished">Keep currently active thumbnail in the center of the screen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="35"/>
      <source>center on active thumbnail</source>
      <translation type="unfinished">center on active thumbnail</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="29"/>
      <source>disable thumbnails</source>
      <translation type="unfinished">disable thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="30"/>
      <source>Disable thumbnails in case no thumbnails are desired whatsoever.</source>
      <translation type="unfinished">Disable thumbnails in case no thumbnails are desired whatsoever.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="35"/>
      <source>disable all thumbnails</source>
      <translation type="unfinished">disable all thumbnails</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="30"/>
      <source>filename label</source>
      <extracomment>A settings title. The filename label here is the one that is written on thumbnails.</extracomment>
      <translation type="unfinished">filename label</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="31"/>
      <source>Show the filename on a small label on the thumbnail image.</source>
      <translation type="unfinished">Show the filename on a small label on the thumbnail image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="57"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="58"/>
      <source>font size:</source>
      <translation type="unfinished">font size:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="30"/>
      <source>filename-only</source>
      <extracomment>A settings title. This refers to using only the filename as thumbnail and no actual image.</extracomment>
      <translation type="unfinished">filename-only</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="31"/>
      <source>Show only the filename as thumbnail, no actual image.</source>
      <translation type="unfinished">Show only the filename as thumbnail, no actual image.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="30"/>
      <source>lift up</source>
      <extracomment>A settings title. This refers to the lift up of thumbnail images when active/hovered.</extracomment>
      <translation type="unfinished">lift up</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="31"/>
      <source>How many pixels to lift up thumbnails when either hovered or active.</source>
      <translation type="unfinished">How many pixels to lift up thumbnails when either hovered or active.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="30"/>
      <source>position</source>
      <extracomment>A settings title referring to the position of the thumbnails (upper or lower edge of PhotoQt).</extracomment>
      <translation type="unfinished">position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="31"/>
      <source>Which edge to show the thumbnails on, upper or lower edge.</source>
      <translation type="unfinished">Which edge to show the thumbnails on, upper or lower edge.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="38"/>
      <source>upper edge</source>
      <extracomment>The upper edge of PhotoQt</extracomment>
      <translation type="unfinished">upper edge</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="40"/>
      <source>lower edge</source>
      <extracomment>The lower edge of PhotoQt</extracomment>
      <translation type="unfinished">lower edge</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="30"/>
      <source>size</source>
      <extracomment>A settings title referring to the size of the thumbnails.</extracomment>
      <translation type="unfinished">size</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="31"/>
      <source>How large (or small) the thumbnails should be.</source>
      <translation type="unfinished">How large (or small) the thumbnails should be.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="30"/>
      <source>spacing</source>
      <extracomment>A settings title referring to the spacing of thumbnails, i.e., how much empty space to have between each.</extracomment>
      <translation type="unfinished">spacing</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="31"/>
      <source>How much space to show between the thumbnails.</source>
      <translation type="unfinished">How much space to show between the thumbnails.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="30"/>
      <source>threads</source>
      <extracomment>A settings title, as in: How many threads to use to generate thumbnails.</extracomment>
      <translation type="unfinished">threads</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="31"/>
      <source>How many threads to use to create thumbnails. Too many threads can slow down your computer!</source>
      <translation type="unfinished">How many threads to use to create thumbnails. Too many threads can slow down your computer!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="50"/>
      <source>Threads:</source>
      <translation type="unfinished">Threads:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="30"/>
      <source>visibility</source>
      <extracomment>A settings title referring to the visibility of the thumbnails, i.e., if and when to hide them.</extracomment>
      <translation type="unfinished">visibility</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="31"/>
      <source>If and how to keep thumbnails visible</source>
      <translation type="unfinished">If and how to keep thumbnails visible</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="37"/>
      <source>hide when not needed</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation type="unfinished">hide when not needed</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="39"/>
      <source>never hide</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation type="unfinished">never hide</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="41"/>
      <source>hide when zoomed in</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation type="unfinished">hide when zoomed in</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="29"/>
      <source>exclude folders</source>
      <translation type="unfinished">exclude folders</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="30"/>
      <source>Exclude the specified folders and all of its subfolders from any sort of caching and preloading.</source>
      <translation type="unfinished">Exclude the specified folders and all of its subfolders from any sort of caching and preloading.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="103"/>
      <source>Do not cache these folders:</source>
      <translation type="unfinished">Do not cache these folders:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="115"/>
      <source>Add folder</source>
      <extracomment>Written on a button</extracomment>
      <translation type="unfinished">Add folder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="110"/>
      <source>One folder per line</source>
      <translation type="unfinished">One folder per line</translation>
    </message>
  </context>
  <context>
    <name>slideshow</name>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="115"/>
      <source>Click to go to the previous image</source>
      <translation type="unfinished">Click to go to the previous image</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="147"/>
      <source>Click to pause slideshow</source>
      <translation type="unfinished">Click to pause slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="148"/>
      <source>Click to play slideshow</source>
      <translation type="unfinished">Click to play slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="176"/>
      <source>Click to go to the next image</source>
      <translation type="unfinished">Click to go to the next image</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="206"/>
      <source>Click to exit slideshow</source>
      <translation type="unfinished">Click to exit slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="256"/>
      <source>Sound volume:</source>
      <translation type="unfinished">Sound volume:</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="61"/>
      <location filename="../qml/slideshow/PQSlideShowSettingsPopout.qml" line="34"/>
      <source>Slideshow settings</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Slideshow settings</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="110"/>
      <source>interval</source>
      <extracomment>The interval between images in a slideshow</extracomment>
      <translation type="unfinished">interval</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="147"/>
      <source>animation</source>
      <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
      <translation type="unfinished">animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="159"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation type="unfinished">opacity</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="161"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation type="unfinished">along x-axis</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="163"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation type="unfinished">along y-axis</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="165"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">rotation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="167"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">explosion</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="169"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">implosion</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="171"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation type="unfinished">choose one at random</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="191"/>
      <source>animation speed</source>
      <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
      <translation type="unfinished">animation speed</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="211"/>
      <source>immediately, without animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation type="unfinished">immediately, without animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="214"/>
      <source>pretty fast animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation type="unfinished">pretty fast animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="217"/>
      <source>not too fast and not too slow</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation type="unfinished">not too fast and not too slow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="219"/>
      <source>very slow animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation type="unfinished">very slow animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="226"/>
      <source>current speed</source>
      <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
      <translation type="unfinished">current speed</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="248"/>
      <source>looping</source>
      <translation type="unfinished">looping</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="261"/>
      <source>loop over all files</source>
      <extracomment>Loop over all images during slideshows</extracomment>
      <translation type="unfinished">loop over all files</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="278"/>
      <source>shuffle</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation type="unfinished">shuffle</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="291"/>
      <source>shuffle all files</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation type="unfinished">shuffle all files</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="308"/>
      <source>subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation type="unfinished">subfolders</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="321"/>
      <source>include images in subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation type="unfinished">include images in subfolders</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="339"/>
      <source>file info</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation type="unfinished">file info</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="352"/>
      <source>hide label with details about current file</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation type="unfinished">hide label with details about current file</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="370"/>
      <source>window buttons</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation type="unfinished">window buttons</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="383"/>
      <source>hide window buttons during slideshow</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation type="unfinished">hide window buttons during slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="401"/>
      <source>music</source>
      <extracomment>The music that is to be played during slideshows</extracomment>
      <translation type="unfinished">music</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="418"/>
      <source>enable music</source>
      <extracomment>Enable music to be played during slideshows</extracomment>
      <translation type="unfinished">enable music</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="425"/>
      <source>no file selected</source>
      <translation type="unfinished">no file selected</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="427"/>
      <source>Click to select music file</source>
      <translation type="unfinished">Click to select music file</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="428"/>
      <source>Click to change music file</source>
      <translation type="unfinished">Click to change music file</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="439"/>
      <source>Common music file formats</source>
      <translation type="unfinished">Common music file formats</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="440"/>
      <source>All Files</source>
      <translation type="unfinished">All Files</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="475"/>
      <source>Start slideshow</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation type="unfinished">Start slideshow</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControlsPopout.qml" line="34"/>
      <source>Slideshow controls</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Slideshow controls</translation>
    </message>
  </context>
  <context>
    <name>startup</name>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="330"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="332"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="334"/>
      <source>Edit with %1</source>
      <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation type="unfinished">Edit with %1</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="336"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="338"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="340"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="342"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="344"/>
      <source>Open in %1</source>
      <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation type="unfinished">Open in %1</translation>
    </message>
  </context>
  <context>
    <name>streaming</name>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="98"/>
      <source>Scan for devices</source>
      <extracomment>Used as tooltip for button that starts a scan for Chromecast streaming devices in the local network</extracomment>
      <translation type="unfinished">Scan for devices</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="206"/>
      <source>Disconnect</source>
      <extracomment>Written on button, as in &apos;Disconnect from connected Chromecast streaming device&apos;</extracomment>
      <translation type="unfinished">Disconnect</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="208"/>
      <source>Connect</source>
      <extracomment>Written on button, as in &apos;Connect to Chromecast streaming device&apos;</extracomment>
      <translation type="unfinished">Connect</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="241"/>
      <source>searching for devices...</source>
      <extracomment>status text while searching for chromecast streaming devices in the local network</extracomment>
      <translation type="unfinished">searching for devices...</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="243"/>
      <source>no devices found</source>
      <extracomment>result of scan for chromecast streaming devices</extracomment>
      <translation type="unfinished">no devices found</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecastPopout.qml" line="34"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Window title</extracomment>
      <translation type="unfinished">Streaming (Chromecast)</translation>
    </message>
  </context>
  <context>
    <name>thumbnailbar</name>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="269"/>
      <source>File size:</source>
      <translation type="unfinished">File size:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="270"/>
      <source>File type:</source>
      <translation type="unfinished">File type:</translation>
    </message>
  </context>
  <context>
    <name>unavailable</name>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="75"/>
      <source>Sorry, but this feature is not yet available on Windows.</source>
      <translation type="unfinished">Sorry, but this feature is not yet available on Windows.</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="34"/>
      <source>Feature unavailable</source>
      <extracomment>Window title, informing user that the requested feature is currently not available</extracomment>
      <translation type="unfinished">Feature unavailable</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailable.qml" line="74"/>
      <source>Sorry, but this feature is not available.</source>
      <translation type="unfinished">Sorry, but this feature is not available.</translation>
    </message>
  </context>
  <context>
    <name>wallpaper</name>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="71"/>
      <source>Warning: %1 module not activated</source>
      <translation type="unfinished">Warning: %1 module not activated</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="80"/>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="69"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="72"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="81"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="69"/>
      <source>Warning: %1 not found</source>
      <translation type="unfinished">Warning: %1 not found</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="102"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="91"/>
      <source>Set to which screens</source>
      <extracomment>As in: Set wallpaper to which screens</extracomment>
      <translation type="unfinished">Set to which screens</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="115"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="103"/>
      <source>Screen</source>
      <extracomment>Used in wallpaper element</extracomment>
      <translation type="unfinished">Screen</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="140"/>
      <source>Set to which workspaces</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation type="unfinished">Set to which workspaces</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="154"/>
      <source>Workspace:</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation type="unfinished">Workspace:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="83"/>
      <location filename="../qml/wallpaper/ele/PQWindows.qml" line="66"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="128"/>
      <source>Choose picture option</source>
      <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
      <translation type="unfinished">Choose picture option</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="57"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="180"/>
      <source>Other</source>
      <extracomment>Used as in: Other Desktop Environment</extracomment>
      <translation type="unfinished">Other</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="100"/>
      <source>Tool:</source>
      <extracomment>Tool refers to a program that can be executed</extracomment>
      <translation type="unfinished">Tool:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="106"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="124"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="142"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="160"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="178"/>
      <source>Click to choose %1</source>
      <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
      <translation type="unfinished">Click to choose %1</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="207"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="226"/>
      <location filename="../qml/wallpaper/PQWallpaperPopout.qml" line="34"/>
      <source>Set as Wallpaper</source>
      <extracomment>Heading of wallpaper element
----------
Written on clickable button
----------
Window title</extracomment>
      <translation type="unfinished">Set as Wallpaper</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQPlasma.qml" line="67"/>
      <source>The image will be set to all screens at the same time.</source>
      <translation type="unfinished">The image will be set to all screens at the same time.</translation>
    </message>
  </context>
</TS>
