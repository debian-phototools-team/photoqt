<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de" sourcelanguage="en">
  <context>
    <name>MainMenu</name>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="308"/>
      <source>Settings</source>
      <translation>Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="232"/>
      <source>Slideshow</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Diaschau</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="86"/>
      <source>Rename file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="91"/>
      <source>Copy file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Datei kopieren</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="96"/>
      <source>Move file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Datei verschieben</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="101"/>
      <source>Delete file</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="114"/>
      <source>Tag faces</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Gesichter markieren</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Hide metadata</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Metadaten verstecken</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="132"/>
      <source>Show metadata</source>
      <translation>Metadaten anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Hide histogram</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Histogramm verstecken</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="137"/>
      <source>Show histogram</source>
      <translation>Histogramm anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="124"/>
      <source>Set as wallpaper</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Als Hintergrundbild setzen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="322"/>
      <source>Quit</source>
      <translation>Beenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="119"/>
      <source>Scale image</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Bild skalieren</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="173"/>
      <source>Navigation</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Navigation</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="177"/>
      <source>Browse images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Bilder durchsuchen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="181"/>
      <source>first</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>erstes</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="184"/>
      <source>last</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>letztes</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="193"/>
      <source>Zoom</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Zoom</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="200"/>
      <source>Fit</source>
      <extracomment>This is an entry in the main menu on the right, used as in &apos;FIT image in window (reset zoom)&apos;. Please keep short!</extracomment>
      <translation>Einpassen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="208"/>
      <source>Rotation/Flip</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Drehung/Spiegelung</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="212"/>
      <source>Horizontal flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Horizontal spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="216"/>
      <source>Vertical flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Vertikal spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="220"/>
      <source>Reset flip</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Spiegelung zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="224"/>
      <source>Reset rotation</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Drehung zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="236"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="249"/>
      <source>Start</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;START slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation>Start</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="240"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="253"/>
      <source>Setup</source>
      <extracomment>This is an entry in the main menu on the right, used as in &quot;SETUP slideshow/sorting&quot;. Please keep short!</extracomment>
      <translation>Einrichten</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="245"/>
      <source>Advanced Sort</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Erweitertes Sortieren</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="258"/>
      <source>Other</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Sonstiges</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="261"/>
      <source>Filter images</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Dateien filtern</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="276"/>
      <source>External</source>
      <extracomment>Used as heading for a group of entries in the main menu on the right. Please keep short!</extracomment>
      <translation>Extern</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="311"/>
      <source>About</source>
      <translation>Über</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="319"/>
      <source>Online help</source>
      <translation>Online-Hilfe</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="124"/>
      <source>Click and drag to resize main menu</source>
      <translation>Klicken und ziehen, um die Größe des Hauptmenüs zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="265"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Streaming (Chromecast)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQContextMenu.qml" line="109"/>
      <source>Copy to clipboard</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="269"/>
      <source>Open in default file manager</source>
      <extracomment>This is an entry in the main menu on the right. Please keep short!</extracomment>
      <translation>Im Dateimanager öffnen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuPopout.qml" line="34"/>
      <source>Main Menu</source>
      <extracomment>Window title</extracomment>
      <translation>Hauptmenü</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="100"/>
      <location filename="../qml/menumeta/PQMainMenuGroup.qml" line="205"/>
      <source>You need to load an image first.</source>
      <translation>Zuerst muss ein Bild geladen werden.</translation>
    </message>
  </context>
  <context>
    <name>PQImageFormats</name>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="48"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="60"/>
      <source>ERROR getting default image formats</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>FEHLER beim Erhalten der voreingestellten Bildformate</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the database of default image formats.</source>
      <translation>Ich habe alles versucht, aber noch nicht einmal eine schreibgeschützte Datenbank der voreingestellten Bildformate kann geladen werden.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="49"/>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Irgendetwas ist schrecklich schief gelaufen!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/imageformats.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default image formats.</source>
      <translation>Ich habe alles versucht, aber die Datenbank mit den voreingestellten Bildformaten kann einfach nicht geladen werden.</translation>
    </message>
  </context>
  <context>
    <name>PQMetaData</name>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="421"/>
      <source>yes</source>
      <extracomment>This string identifies that flash was fired, stored in image metadata</extracomment>
      <translation>ja</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="423"/>
      <source>no</source>
      <extracomment>This string identifies that flash was not fired, stored in image metadata</extracomment>
      <translation>nein</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="425"/>
      <source>No flash function</source>
      <extracomment>This string refers to the absense of a flash, stored in image metadata</extracomment>
      <translation>Keine Blitzfunktion</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="427"/>
      <source>strobe return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Stroboskop-Lichtreflexion nicht erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="429"/>
      <source>strobe return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Stroboskop-Lichtreflexion erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="431"/>
      <source>compulsory flash mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>obligatorischer Blitzmodus</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="433"/>
      <source>auto mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>automatisch</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="435"/>
      <source>red-eye reduction mode</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Rote-Augen-Reduktionsmodus</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="437"/>
      <source>return light detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Lichtreflexion erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="439"/>
      <source>return light not detected</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Lichtreflexion nicht erkannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="487"/>
      <source>Invalid flash</source>
      <extracomment>This string refers to a flash mode, stored in image metadata</extracomment>
      <translation>Ungültiger Blitz</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="498"/>
      <source>Standard</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Standard</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="501"/>
      <source>Landscape</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Querformat</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="504"/>
      <source>Portrait</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Hochformat</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="507"/>
      <source>Night Scene</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Nachtszene</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="510"/>
      <source>Invalid Scene Type</source>
      <extracomment>This string refers to a type of scene, stored in image metadata</extracomment>
      <translation>Ungültiger Szenentyp</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="563"/>
      <source>Unknown</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Unbekannt</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="566"/>
      <source>Daylight</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tageslicht</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="569"/>
      <source>Fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluoreszierend</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="572"/>
      <source>Tungsten (incandescent light)</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Tungsten (Glühbirne)</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="575"/>
      <source>Flash</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Blitz</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="578"/>
      <source>Fine weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Heiteres Wetter</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="581"/>
      <source>Cloudy Weather</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Bewölktes Wetter</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="584"/>
      <source>Shade</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Schatten</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="587"/>
      <source>Daylight fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Fluoreszierendes Tageslicht</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="590"/>
      <source>Day white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Weißes fluoreszierendes Tageslicht</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="593"/>
      <source>Cool white fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Weißes fluoreszierendes kaltes Licht</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="596"/>
      <source>White fluorescent</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Weißes fluoreszierendes Licht</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="599"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="602"/>
      <location filename="../cplusplus/scripts/metadata.cpp" line="605"/>
      <source>Standard light</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Standard Licht</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="618"/>
      <source>Other light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Andere Lichtquelle</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/metadata.cpp" line="621"/>
      <source>Invalid light source</source>
      <extracomment>This string refers to the light source stored in image metadata</extracomment>
      <translation>Ungültige Lichtquelle</translation>
    </message>
  </context>
  <context>
    <name>PQPrintSupport</name>
    <message>
      <location filename="../cplusplus/print/printsupport.cpp" line="52"/>
      <source>Print</source>
      <translation>Drucken</translation>
    </message>
  </context>
  <context>
    <name>PQSettings</name>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="64"/>
      <source>ERROR getting database with default settings</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>FEHLER beim Abrufen der Datenbank mit Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="65"/>
      <source>I tried hard, but I just cannot open even a read-only version of the settings database.</source>
      <translation>Ich habe alles versucht, aber ich kann nicht einmal eine schreibgeschützte Version der Einstellungsdatenbank öffnen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="65"/>
      <location filename="../cplusplus/settings/settings.cpp" line="77"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Irgendetwas ist schrecklich schief gelaufen!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="76"/>
      <source>ERROR opening database with default settings</source>
      <translation>FEHLER beim Öffnen der Datenbank mit Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/settings.cpp" line="77"/>
      <source>I tried hard, but I just cannot open the database of default settings.</source>
      <translation>Ich habe alles versucht, aber die Datenbank mit den Standardeinstellungen kann einfach nicht geladen werden.</translation>
    </message>
  </context>
  <context>
    <name>PQShortcuts</name>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="48"/>
      <source>ERROR getting database with default shortcuts</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>FEHLER beim Abrufen der Datenbank mit Standard-Kurzbefehlen</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <source>I tried hard, but I just cannot open even a read-only version of the shortcuts database.</source>
      <translation>Ich habe alles versucht, aber ich kann nicht einmal eine schreibgeschützte Version der Datenbank für die Kurzbefehle öffnen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="49"/>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>Something went terribly wrong somewhere!</source>
      <translation>Irgendetwas ist schrecklich schief gelaufen!</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="60"/>
      <source>ERROR opening database with default settings</source>
      <translation>FEHLER beim Öffnen der Datenbank mit Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../cplusplus/settings/shortcuts.cpp" line="61"/>
      <source>I tried hard, but I just cannot open the database of default shortcuts.</source>
      <translation>Ich habe alles versucht, aber die Datenbank mit den voreingestellten Kurzbefehlen kann einfach nicht geladen werden.</translation>
    </message>
  </context>
  <context>
    <name>PQStartup</name>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="56"/>
      <source>SQLite error</source>
      <extracomment>This is the window title of an error message box</extracomment>
      <translation>SQLite Fehler</translation>
    </message>
    <message>
      <location filename="../cplusplus/startup/startup.cpp" line="57"/>
      <source>You seem to be missing the SQLite driver for Qt. This is needed though for a few different things, like reading and writing the settings. Without it, PhotoQt cannot function!</source>
      <translation>Der Qt Treiber für SQLite scheint zu fehlen. Dieser wird benötigt für ein paar verschiedene Dinge, z. B. das Lesen und Schreiben der Einstellungen. Ohne ihn kann PhotoQt nicht funktionieren!</translation>
    </message>
  </context>
  <context>
    <name>TabShortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/TabShortcuts.qml" line="245"/>
      <source/>
      <translation type="unfinished"/>
    </message>
  </context>
  <context>
    <name>about</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="52"/>
      <location filename="../qml/about/PQAbout.qml" line="165"/>
      <source>Close</source>
      <translation>Schließen</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="98"/>
      <source>Current version:</source>
      <translation>Version:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="100"/>
      <source>Show configuration overview</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Zeige Konfigurationsübersicht</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="110"/>
      <source>License:</source>
      <translation>Lizenz:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="115"/>
      <source>Open license in browser</source>
      <translation>Lizenz im Browser öffnen</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="135"/>
      <source>Open website in browser</source>
      <translation>Webseite im Browser öffnen</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="203"/>
      <source>Configuration</source>
      <extracomment>The &apos;configuration&apos; talked about here refers to the configuration at compile time, i.e., which image libraries were enabled and which versions</extracomment>
      <translation>Konfiguration</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="222"/>
      <source>Copy to clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="130"/>
      <source>Website:</source>
      <translation>Webseite:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="145"/>
      <source>Contact:</source>
      <translation>Kontakt:</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="150"/>
      <source>Send an email</source>
      <translation>Sende eine E-Mail</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAboutPopout.qml" line="34"/>
      <source>About</source>
      <extracomment>Window title</extracomment>
      <translation>Über</translation>
    </message>
  </context>
  <context>
    <name>advancedsort</name>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="83"/>
      <location filename="../qml/advancedsort/PQAdvancedSortPopout.qml" line="34"/>
      <source>Advanced Image Sort</source>
      <extracomment>Window title</extracomment>
      <translation>Erweiterte Bildsortierung</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="94"/>
      <source>It is possible to sort the images in the current folder by color properties. Depending on the number of images and the settings, this might take a few seconds.</source>
      <translation>Es ist möglich, die Bilder im aktuellen Ordner nach Farbeigenschaften zu sortieren. Abhängig von der Anzahl der Bilder und Einstellungen kann dies ein paar Sekunden dauern.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="107"/>
      <source>Sort by:</source>
      <extracomment>Used as &apos;sort by dominant/average color&apos;</extracomment>
      <translation>Sortieren nach:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="116"/>
      <source>Resolution</source>
      <extracomment>The image resolution (width/height in pixels)</extracomment>
      <translation>Auflösung</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="118"/>
      <source>Dominant color</source>
      <extracomment>The color that is most common in the image</extracomment>
      <translation>Dominante Farbe</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="120"/>
      <source>Average color</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Durchschnittsfarbe</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="122"/>
      <source>Luminosity</source>
      <extracomment>the average color of the image</extracomment>
      <translation>Helligkeit</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="137"/>
      <source>ascending</source>
      <extracomment>sort order, i.e., &apos;ascending order&apos;</extracomment>
      <translation>aufsteigend</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="142"/>
      <source>descending</source>
      <extracomment>sort order, i.e., &apos;descending order&apos;</extracomment>
      <translation>absteigend</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="159"/>
      <source>speed vs quality:</source>
      <extracomment>Please keep short! Sorting images by color comes with a speed vs quality tradeoff.</extracomment>
      <translation>Geschwindigkeit gegen Qualität:</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="165"/>
      <source>low quality (fast)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>geringe Qualität (schnell)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="167"/>
      <source>medium quality</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>mittlere Qualität</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="169"/>
      <source>high quality (slow)</source>
      <extracomment>quality and speed of sorting image by color</extracomment>
      <translation>hohe Qualität (langsam)</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="181"/>
      <source>There is also a quickstart shortcut that immediately starts the sorting using the latest settings.</source>
      <translation>Es gibt auch einen Schnellstart-Kurzbefehl, der die Sortierung mit den letzten Einstellungen sofort startet.</translation>
    </message>
    <message>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="201"/>
      <source>Sort images</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Bilder sortieren</translation>
    </message>
  </context>
  <context>
    <name>buttongeneric</name>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="81"/>
      <source>Ok</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>OK</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="83"/>
      <source>Cancel</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Abbrechen</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="85"/>
      <source>Save</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Speichern</translation>
    </message>
    <message>
      <location filename="../qml/elements/PQButton.qml" line="87"/>
      <source>Close</source>
      <extracomment>This is a generic string written on clickable buttons - please keep short!</extracomment>
      <translation>Schließen</translation>
    </message>
  </context>
  <context>
    <name>commandlineparser</name>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="51"/>
      <source>Image Viewer</source>
      <translation>Bildbetrachter</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="53"/>
      <source>Image file to open.</source>
      <translation>Zu öffnendes Bild.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="60"/>
      <source>Make PhotoQt ask for a new file.</source>
      <translation>PhotoQt zwingen nach einer neuen Datei zu fragen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="62"/>
      <source>Shows PhotoQt from system tray.</source>
      <translation>Zeigt PhotoQt aus der Systemleiste an.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="64"/>
      <source>Hides PhotoQt to system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>Versteckt PhotoQt in die Systemleiste.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="66"/>
      <source>Show/Hide PhotoQt.</source>
      <translation>PhotoQt zeigen/verstecken.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="68"/>
      <source>Enable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Miniaturbilder aktivieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="70"/>
      <source>Disable thumbnails.</source>
      <extracomment>Command line option</extracomment>
      <translation>Miniaturbilder deaktivieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="72"/>
      <source>Enable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Symbol in der Systemleiste anzeigen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="74"/>
      <source>Disable system tray icon.</source>
      <extracomment>Command line option</extracomment>
      <translation>Symbol in der Systemleiste verstecken.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="76"/>
      <source>Start PhotoQt hidden to the system tray.</source>
      <extracomment>Command line option</extracomment>
      <translation>PhotoQt versteckt in der Systemleiste starten.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="78"/>
      <source>Open standalone PhotoQt, allows for multiple instances but without remote interaction.</source>
      <extracomment>Command line option</extracomment>
      <translation>PhotoQt standalone starten, dies erlaubt mehrere Instanzen gleichzeitig aber ohne externe Interaktion.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="80"/>
      <source>Simulate a shortcut sequence</source>
      <extracomment>Command line option</extracomment>
      <translation>Einen Kurzbefehl simulieren</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="82"/>
      <source>Switch on debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Debugmeldungen einschalten.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="84"/>
      <source>Switch off debug messages.</source>
      <extracomment>Command line option</extracomment>
      <translation>Debugmeldungen ausschalten.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="86"/>
      <source>Export configuration to given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Konfiguration in angegebene Datei exportieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="88"/>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="92"/>
      <source>filename</source>
      <extracomment>Command line option</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="90"/>
      <source>Import configuration from given filename.</source>
      <extracomment>Command line option</extracomment>
      <translation>Konfiguration von angegebener Datei importieren.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="94"/>
      <source>Check the configuration and correct any detected issues.</source>
      <extracomment>Command line option</extracomment>
      <translation>Prüft die Konfiguration und korrigiert alle gefundenen Probleme.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="96"/>
      <source>Reset default configuration.</source>
      <extracomment>Command line option</extracomment>
      <translation>Konfiguration auf Standardwerte zurücksetzen.</translation>
    </message>
    <message>
      <location filename="../cplusplus/singleinstance/commandlineparser.cpp" line="98"/>
      <source>Show configuration overview.</source>
      <extracomment>Command line option</extracomment>
      <translation>Zeige Konfigurationsübersicht.</translation>
    </message>
  </context>
  <context>
    <name>facetagging</name>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="203"/>
      <source>Who is this?</source>
      <extracomment>This question is asked in the face tagger to ask for the name of a tagged face</extracomment>
      <translation>Wer ist das?</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagger.qml" line="210"/>
      <source>Enter name</source>
      <translation>Namen eingeben</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="62"/>
      <source>Click to exit face tagging mode</source>
      <translation>Klicken, um den Modus zum Markieren von Gesichtern zu verlassen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="75"/>
      <source>Click to tag faces, changes are saved automatically</source>
      <translation>Klicken, um Gesichter zu markieren, Änderungen werden automatisch gespeichert</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/image/PQFaceTagsUnsupported.qml" line="60"/>
      <source>File type does not support face tags.</source>
      <translation>Dateityp unterstützt das Markieren von Gesichtern nicht.</translation>
    </message>
  </context>
  <context>
    <name>filedialog</name>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="71"/>
      <source>Backwards</source>
      <translation>Zurück</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="98"/>
      <source>Up a level</source>
      <translation>Eine Ebene nach oben</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="124"/>
      <source>Forwards</source>
      <translation>Vorwärts</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="199"/>
      <source>List subfolders</source>
      <translation>Unterordner auflisten</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="199"/>
      <source>No subfolders found</source>
      <translation>Keine Unterordner gefunden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="289"/>
      <source>Enter fullscreen</source>
      <translation>Vollbildmodus aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="290"/>
      <source>Exit fullscreen</source>
      <translation>Vollbildmodus beenden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQBreadCrumbs.qml" line="319"/>
      <source>Close</source>
      <translation>Schließen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="109"/>
      <source>Storage devices</source>
      <extracomment>This is the category title of storage devices to open (like USB keys) in the element for opening files</extracomment>
      <translation>Speichermedien</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQDevices.qml" line="141"/>
      <source>Detected storage devices on your system</source>
      <translation>Erkannte Speichergeräte auf deinem System</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="118"/>
      <source>no supported files/folders found</source>
      <translation>keine unterstützten Dateien/Ordner gefunden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="232"/>
      <source>Click and drag to favorites</source>
      <translation>Klicke und ziehe zu Favoriten</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="360"/>
      <source># images</source>
      <translation># Bilder</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="361"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="380"/>
      <source>Date:</source>
      <translation>Datum:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="362"/>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="381"/>
      <source>Time:</source>
      <translation>Zeit:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="378"/>
      <source>File size:</source>
      <translation>Dateigröße:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="379"/>
      <source>File type:</source>
      <translation>Dateityp:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="463"/>
      <source>%1 image</source>
      <translation>%1 Bild</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQFileView.qml" line="465"/>
      <source>%1 images</source>
      <translation>%1 Bilder</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="51"/>
      <source>no folder added to favorites yet</source>
      <extracomment>&apos;favorites&apos; here refers to the list of favorite folders a user can set in the file dialog</extracomment>
      <translation>noch kein Ordner zu den Favoriten hinzugefügt</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="157"/>
      <source>Favorites</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Favoriten</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="173"/>
      <source>Your favorites</source>
      <translation>Deine Favoriten</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Show entry</source>
      <translation>Eintrag anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="222"/>
      <source>Hide entry</source>
      <translation>Eintrag verstecken</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="223"/>
      <source>Remove entry</source>
      <translation>Eintrag entfernen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="224"/>
      <source>Hide hidden entries</source>
      <translation>Versteckte Einträge verbergen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQPlaces.qml" line="224"/>
      <source>Show hidden entries</source>
      <translation>Versteckte Einträge anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this folder</source>
      <translation>Diesen Ordner laden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="43"/>
      <source>Load this file</source>
      <translation>Diese Datei laden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="56"/>
      <source>Add to Favorites</source>
      <translation>Zu den Favoriten hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="78"/>
      <source>Show tooltip with image details</source>
      <translation>Tooltip mit Bilddetails anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="88"/>
      <source>Visible</source>
      <extracomment>This is a context menu entry, referring to whether the large preview image is VISIBLE</extracomment>
      <translation>Sichtbar</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="96"/>
      <source>Higher resolution</source>
      <extracomment>This is a context menu entry, referring to whether a preview image with a HIGHER RESOLUTION should be loaded</extracomment>
      <translation>Höhere Auflösung</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="104"/>
      <source>Blurred image</source>
      <extracomment>This is a context menu entry, selecting it will BLUR the preview IMAGE</extracomment>
      <translation>Verschwommenes Bild</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="112"/>
      <source>Muted colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image MUTED</extracomment>
      <translation>Gedämpfte Farben</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="120"/>
      <source>Full colors</source>
      <extracomment>This is a context menu entry, selecting it will make the COLORS of the preview image in FULL (i.e., without blur/opacity)</extracomment>
      <translation>Volle Farben</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="64"/>
      <source>Show hidden files</source>
      <translation>Versteckte Dateien anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQRightClickMenu.qml" line="71"/>
      <source>Show thumbnails</source>
      <translation>Miniaturbilder zeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="103"/>
      <source>Standard</source>
      <extracomment>This is the category title of user-set folders (or favorites) in the file dialog</extracomment>
      <translation>Standard</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQStandard.qml" line="119"/>
      <source>Some standard locations</source>
      <translation>Einige Standardorte</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="48"/>
      <source>Zoom:</source>
      <translation>Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="57"/>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="72"/>
      <source>Adjust font size of files and folders</source>
      <translation>Schriftgröße für Dateien und Ordner anpassen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="73"/>
      <source>Zoom factor:</source>
      <translation>Zoomfaktor:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="92"/>
      <source>Sort by:</source>
      <translation>Sortieren nach:</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="94"/>
      <source>Name</source>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="95"/>
      <source>Natural Name</source>
      <translation>Natürlicher Name</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="96"/>
      <source>Time modified</source>
      <translation>Bearbeitungszeit</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="97"/>
      <source>File size</source>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="98"/>
      <source>File type</source>
      <translation>Dateityp</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="99"/>
      <source>reverse order</source>
      <translation>Reihenfolge umkehren</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="106"/>
      <source>Choose by what to sort the files</source>
      <translation>Wie die Dateien sortiert werden sollen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="142"/>
      <source>All supported images</source>
      <translation>Alle unterstützten Bilder</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="147"/>
      <source>Video files</source>
      <translation>Videodateien</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="148"/>
      <source>All files</source>
      <translation>Alle Dateien</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="158"/>
      <source>Choose which selection of files to show</source>
      <translation>Wähle welche Dateien angezeigt werden sollen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="178"/>
      <source>Remember loaded folder between sessions.</source>
      <translation>Geladenen Ordner zwischen den Sitzungen speichern.</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="200"/>
      <source>Switch between list and icon view</source>
      <translation>Zwischen Listen- und Symbolansicht wechseln</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/parts/PQTweaks.qml" line="230"/>
      <source>Open global settings</source>
      <translation>Globale Einstellungen öffnen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Hide standard locations</source>
      <translation>Standardorte ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="116"/>
      <source>Show standard locations</source>
      <translation>Standardorte anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Hide favorite locations</source>
      <translation>Favoriten ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="117"/>
      <source>Show favorite locations</source>
      <translation>Favoriten anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Hide storage devices</source>
      <translation>Speichergeräte ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="118"/>
      <source>Show storage devices</source>
      <translation>Speichergeräte anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/filedialog/PQFileDialogPopout.qml" line="34"/>
      <source>File dialog</source>
      <extracomment>Window title</extracomment>
      <translation>Dateidialog</translation>
    </message>
  </context>
  <context>
    <name>filemanagement</name>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="83"/>
      <location filename="../qml/filemanagement/PQDeletePopout.qml" line="34"/>
      <source>Delete file?</source>
      <extracomment>Window title</extracomment>
      <translation>Datei löschen?</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="101"/>
      <source>An error occured, file could not be deleted!</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht gelöscht werden!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="121"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="177"/>
      <source>Move to trash</source>
      <translation>In den Papierkorb verschieben</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQDelete.qml" line="137"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="180"/>
      <source>Delete permanently</source>
      <translation>Dauerhaft löschen</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="83"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="132"/>
      <location filename="../qml/filemanagement/PQRenamePopout.qml" line="34"/>
      <source>Rename file</source>
      <extracomment>Window title</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="101"/>
      <source>An error occured, file could not be renamed!</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht umbenannt werden!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQRename.qml" line="112"/>
      <source>Enter new filename</source>
      <translation>Neuen Dateiname eingeben</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="82"/>
      <location filename="../qml/filemanagement/PQSaveAsPopout.qml" line="34"/>
      <source>Save file as</source>
      <extracomment>This is a title, similar to all the &apos;save as&apos; options in many programs.
----------
Window title</extracomment>
      <translation>Datei speichern als</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="105"/>
      <source>An error occured, file could not be saved!</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht gespeichert werden!</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="116"/>
      <source>Operation cancelled</source>
      <extracomment>&apos;Operation&apos; here is the operation of saving an image in a new format</extracomment>
      <translation>Vorgang abgebrochen</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="124"/>
      <source>Filter formats</source>
      <extracomment>This is a short hint informing the user that here they can &apos;filter all the possible file formats&apos;</extracomment>
      <translation>Formate filtern</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="223"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="230"/>
      <source>New filename</source>
      <translation>Neuer Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="251"/>
      <source>Choose location and save file</source>
      <translation>Ort auswählen und Datei speichern</translation>
    </message>
  </context>
  <context>
    <name>filter</name>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="90"/>
      <source>Filter images in current directory</source>
      <translation>Bilder im aktuellen Verzeichnis filtern</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="102"/>
      <source>To filter by file extension, start the term with a dot. Setting the width or height of the resolution to 0 ignores that dimension.</source>
      <translation>Um nach Dateierweiterung zu filtern, starte den Begriff mit einem Punkt. Wenn die Breite oder Höhe der Auflösung auf 0 gesetzt ist, dann wird diese Dimension ignoriert.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="120"/>
      <source>File name/extension:</source>
      <translation>Dateiname/-erweiterung:</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="132"/>
      <source>Enter terms</source>
      <translation>Begriffe eingeben</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="161"/>
      <location filename="../qml/filter/PQFilter.qml" line="213"/>
      <source>greater than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution GREATER THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size GREATER THAN 123 KB/MB&apos;</extracomment>
      <translation>größer als</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="163"/>
      <location filename="../qml/filter/PQFilter.qml" line="215"/>
      <source>less than</source>
      <extracomment>used as tooltip in the sense of &apos;image resolution LESS THAN 123x123&apos;
----------
used as tooltip in the sense of &apos;file size LESS THAN 123 KB/MB&apos;</extracomment>
      <translation>kleiner als</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="200"/>
      <source>File size</source>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="260"/>
      <source>Please note that filtering by image resolution can take a little while, depending on the number of images in the folder.</source>
      <translation>Bitte beachte, dass das Filtern nach Bildauflösung eine Weile dauern kann, abhängig von der Anzahl der Bilder im Ordner.</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="283"/>
      <location filename="../qml/filter/PQFilterPopout.qml" line="34"/>
      <source>Filter</source>
      <extracomment>Written on a clickable button - please keep short
----------
Window title</extracomment>
      <translation>Filter</translation>
    </message>
    <message>
      <location filename="../qml/filter/PQFilter.qml" line="305"/>
      <source>Remove filter</source>
      <extracomment>Written on a clickable button - please keep short</extracomment>
      <translation>Filter entfernen</translation>
    </message>
  </context>
  <context>
    <name>histogram</name>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="143"/>
      <location filename="../qml/histogram/PQHistogramPopout.qml" line="34"/>
      <source>Histogram</source>
      <extracomment>Window title</extracomment>
      <translation>Histogramm</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="145"/>
      <source>Loading...</source>
      <extracomment>As in: Loading the histogram for the current image</extracomment>
      <translation>Lade...</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Click-and-drag to move.</source>
      <extracomment>Used for the histogram. The version refers to the type of histogram that is available (colored and greyscale)</extracomment>
      <translation>Zum Verschieben klicken und ziehen.</translation>
    </message>
    <message>
      <location filename="../qml/histogram/PQHistogram.qml" line="175"/>
      <source>Right click to switch version.</source>
      <translation>Rechtsklick, um Version zu wechseln.</translation>
    </message>
  </context>
  <context>
    <name>imageprovider</name>
    <message>
      <location filename="../cplusplus/imageprovider/imageproviderfull.cpp" line="65"/>
      <location filename="../cplusplus/imageprovider/imageproviderthumb.cpp" line="147"/>
      <source>File failed to load, it does not exist!</source>
      <translation>Laden der Datei fehlgeschlagen, sie existiert nicht!</translation>
    </message>
  </context>
  <context>
    <name>imgur</name>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="86"/>
      <location filename="../qml/imgur/PQImgurPopout.qml" line="34"/>
      <source>Upload to imgur.com</source>
      <extracomment>Window title</extracomment>
      <translation>Hochladen auf imgur.com</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="97"/>
      <source>anonymous</source>
      <extracomment>Used as in: Upload image as anonymous user</extracomment>
      <translation>anonym</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="129"/>
      <source>Obtaining image url...</source>
      <translation>Bild-URL wird abgerufen...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="140"/>
      <source>This seems to take a long time...</source>
      <translation>Dies scheint eine lange Zeit zu dauern...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="141"/>
      <source>There might be a problem with your internet connection or the imgur.com servers.</source>
      <translation>Es könnte ein Problem mit deiner Internetverbindung oder den imgur.com-Servern geben.</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="152"/>
      <source>An Error occurred while uploading image!</source>
      <translation>Beim Hochladen des Bildes ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="153"/>
      <source>Error code:</source>
      <translation>Fehlercode:</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="164"/>
      <source>You do not seem to be connected to the internet...</source>
      <translation>Du scheinst nicht mit dem Internet verbunden zu sein...</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="165"/>
      <source>Unable to upload!</source>
      <translation>Hochladen nicht möglich!</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="187"/>
      <source>Access Image</source>
      <translation>Bild aufrufen</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="200"/>
      <location filename="../qml/imgur/PQImgur.qml" line="232"/>
      <source>Click to open in browser</source>
      <translation>Zum Öffnen im Browser klicken</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="207"/>
      <location filename="../qml/imgur/PQImgur.qml" line="239"/>
      <source>Copy to clipboard</source>
      <translation>In die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/imgur/PQImgur.qml" line="219"/>
      <source>Delete Image</source>
      <translation>Bild löschen</translation>
    </message>
  </context>
  <context>
    <name>keymouse</name>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="30"/>
      <source>Alt</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Alt</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="32"/>
      <source>Ctrl</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Strg</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="34"/>
      <source>Shift</source>
      <extracomment>Refers to a keyboard modifier</extracomment>
      <translation>Umschalt</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="36"/>
      <source>Page Up</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Bild Hoch</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="38"/>
      <source>Page Down</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Bild Runter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="40"/>
      <source>Meta</source>
      <extracomment>Refers to the key that usually has the Windows symbol on it</extracomment>
      <translation>Meta</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="42"/>
      <source>Keypad</source>
      <extracomment>Refers to the key that triggers the number block on keyboards</extracomment>
      <translation>Ziffernblock</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="44"/>
      <source>Escape</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Esc</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="46"/>
      <source>Right</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Rechts</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="48"/>
      <source>Left</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Links</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="50"/>
      <source>Up</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Hoch</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="52"/>
      <source>Down</source>
      <extracomment>Refers to one of the arrow keys on the keyboard</extracomment>
      <translation>Runter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="54"/>
      <source>Space</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Leertaste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="56"/>
      <source>Delete</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Löschen</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="58"/>
      <source>Backspace</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Rücktaste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="60"/>
      <source>Home</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Pos 1</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="62"/>
      <source>End</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Ende</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="64"/>
      <source>Insert</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Einfg</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="66"/>
      <source>Tab</source>
      <extracomment>Refers to one of the keys on the keyboard</extracomment>
      <translation>Tab</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="69"/>
      <source>Return</source>
      <extracomment>Return refers to the enter key of the number block - please try to make the translations of Return and Enter (the main button) different if possible!</extracomment>
      <translation>Enter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="72"/>
      <source>Enter</source>
      <extracomment>Enter refers to the main enter key - please try to make the translations of Return (in the number block) and Enter different if possible!</extracomment>
      <translation>Eingabe</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="78"/>
      <source>Left Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Linke Taste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="80"/>
      <source>Right Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Rechte Taste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="82"/>
      <source>Middle Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Mittlere Taste</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="84"/>
      <source>Back Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Zurück</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="86"/>
      <source>Forward Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Vorwärts</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="88"/>
      <source>Task Button</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Task</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="90"/>
      <source>Button #7</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #7</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="92"/>
      <source>Button #8</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #8</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="94"/>
      <source>Button #9</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #9</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="96"/>
      <source>Button #10</source>
      <extracomment>Refers to a mouse button</extracomment>
      <translation>Taste #10</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="98"/>
      <source>Double Click</source>
      <extracomment>Refers to a mouse event</extracomment>
      <translation>Doppelklick</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="100"/>
      <source>Wheel Up</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Mausrad hoch</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="102"/>
      <source>Wheel Down</source>
      <extracomment>Refers to the mouse wheel</extracomment>
      <translation>Mausrad runter</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="104"/>
      <source>East</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Osten</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="106"/>
      <source>South</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Süden</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="108"/>
      <source>West</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Westen</translation>
    </message>
    <message>
      <location filename="../qml/shortcuts/PQKeyMouseStrings.qml" line="110"/>
      <source>North</source>
      <extracomment>Refers to a direction of the mouse when performing a mouse gesture</extracomment>
      <translation>Norden</translation>
    </message>
  </context>
  <context>
    <name>logging</name>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="33"/>
      <source>Logging</source>
      <extracomment>Window title</extracomment>
      <translation>Protokollierung</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="109"/>
      <source>enable debug messages</source>
      <translation>Debugmeldungen aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="134"/>
      <source>copy to clipboard</source>
      <translation>in die Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/logging/PQLogging.qml" line="135"/>
      <source>save to file</source>
      <translation>In Datei speichern</translation>
    </message>
  </context>
  <context>
    <name>metadata</name>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="90"/>
      <source>File name</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="92"/>
      <source>Dimensions</source>
      <extracomment>The dimensions of the loaded image. Please keep string short!</extracomment>
      <translation>Bildgröße</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="96"/>
      <source>File size</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="98"/>
      <source>File type</source>
      <extracomment>Please keep string short!</extracomment>
      <translation>Dateityp</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="101"/>
      <source>Make</source>
      <extracomment>Exif image metadata: the make of the camera used to take the photo. Please keep string short!</extracomment>
      <translation>Hersteller</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="103"/>
      <source>Model</source>
      <extracomment>Exif image metadata: the model of the camera used to take the photo. Please keep string short!</extracomment>
      <translation>Modell</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="105"/>
      <source>Software</source>
      <extracomment>Exif image metadata: the software used to create the photo. Please keep string short!</extracomment>
      <translation>Software</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="108"/>
      <source>Time Photo was Taken</source>
      <extracomment>Exif image metadata: when the photo was taken. Please keep string short!</extracomment>
      <translation>Aufnahmezeit</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="110"/>
      <source>Exposure Time</source>
      <extracomment>Exif image metadata: how long the sensor was exposed to the light. Please keep string short!</extracomment>
      <translation>Belichtungszeit</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="112"/>
      <source>Flash</source>
      <extracomment>Exif image metadata: the flash setting when the photo was taken. Please keep string short!</extracomment>
      <translation>Blitz</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="115"/>
      <source>Scene Type</source>
      <extracomment>Exif image metadata: the specific scene type the camera used for the photo. Please keep string short!</extracomment>
      <translation>Szenenart</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="117"/>
      <source>Focal Length</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/Focal_length . Please keep string short!</extracomment>
      <translation>Brennweite</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="160"/>
      <location filename="../qml/menumeta/PQMetaDataPopout.qml" line="34"/>
      <source>Metadata</source>
      <extracomment>This is the heading of the metadata element
----------
Window title</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="199"/>
      <source>No File Loaded</source>
      <translation>Keine Datei geladen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="252"/>
      <source>Click to open GPS position with online map</source>
      <translation>Klicken, um GPS-Position mit Online-Karte zu öffnen</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="353"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="385"/>
      <source>Click and drag to resize element</source>
      <translation>Klicken und ziehen, um die Größe des Elements zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="121"/>
      <source>Light Source</source>
      <extracomment>Exif image metadata: What type of light the camera detected. Please keep string short!</extracomment>
      <translation>Lichtquelle</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="94"/>
      <source>Image</source>
      <extracomment>Used as in &quot;Image 3/16&quot;. The numbers (position of image in folder) are added on automatically. Please keep string short!</extracomment>
      <translation>Bild</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="124"/>
      <source>Keywords</source>
      <extracomment>IPTC image metadata: A description of the image by the user/software. Please keep string short!</extracomment>
      <translation>Schlüsselwörter</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="126"/>
      <source>Location</source>
      <extracomment>IPTC image metadata: The CITY and COUNTRY the imge was taken in. Please keep string short!</extracomment>
      <translation>Ort</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="128"/>
      <source>Copyright</source>
      <extracomment>IPTC image metadata. Please keep string short!</extracomment>
      <translation>Urheberrecht</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="131"/>
      <source>GPS Position</source>
      <extracomment>Exif image metadata. Please keep string short!</extracomment>
      <translation>GPS-Position</translation>
    </message>
    <message>
      <location filename="../qml/menumeta/PQMetaData.qml" line="119"/>
      <source>F Number</source>
      <extracomment>Exif image metadata: https://en.wikipedia.org/wiki/F-number . Please keep string short!</extracomment>
      <translation>Blende</translation>
    </message>
  </context>
  <context>
    <name>navigate</name>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="66"/>
      <source>Click and drag to move</source>
      <translation>Zum Verschieben klicken und ziehen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="99"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="71"/>
      <source>Navigate to previous image in folder</source>
      <translation>Vorhergehendes Bild im Ordner anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="128"/>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="93"/>
      <source>Navigate to next image in folder</source>
      <translation>Nächstes Bild im Ordner anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQNavigation.qml" line="154"/>
      <source>Show main menu</source>
      <translation>Hauptmenü anzeigen</translation>
    </message>
  </context>
  <context>
    <name>other</name>
    <message>
      <location filename="../qml/mainwindow/PQMessage.qml" line="76"/>
      <source>Open a file to start</source>
      <translation>Öffne eine Datei zum Starten</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="114"/>
      <source>Click anywhere to open a file</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Zum Öffnen einer Datei irgendwo klicken</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="124"/>
      <source>Move your cursor to:</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>Bewege deinen Cursor zum:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="134"/>
      <source>RIGHT EDGE for the main menu</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, first option for where to move cursor to</extracomment>
      <translation>RECHTEN RAND für das Hauptmenü</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="145"/>
      <source>LEFT EDGE for the metadata</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, second option for where to move cursor to</extracomment>
      <translation>LINKEN RAND für die Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="156"/>
      <source>BOTTOM EDGE to show the thumbnails</source>
      <extracomment>Part of the message shown in the main view before any image is loaded, third option for where to move cursor to</extracomment>
      <translation>UNTEREN RAND um die Vorschaubilder zu zeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="166"/>
      <source>(once an image/folder is loaded)</source>
      <extracomment>Part of the message shown in the main view before any image is loaded</extracomment>
      <translation>(sobald ein Bild/Ordner geladen ist)</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="221"/>
      <source>No matches found</source>
      <extracomment>Used as in: No matches found for the currently set filter</extracomment>
      <translation>Keine Treffer gefunden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow.qml" line="233"/>
      <source>PhotoQt Image Viewer</source>
      <extracomment>The window title of PhotoQt</extracomment>
      <translation>PhotoQt Bildbetrachter</translation>
    </message>
  </context>
  <context>
    <name>popinpopout</name>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="270"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="295"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="289"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="230"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="230"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="213"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="345"/>
      <location filename="../qml/filter/PQFilter.qml" line="361"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="298"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="425"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="304"/>
      <location filename="../qml/scale/PQScale.qml" line="329"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="371"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="532"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="403"/>
      <source>Merge into main interface</source>
      <extracomment>Tooltip of small button to merge a popped out element (i.e., one in its own window) into the main interface</extracomment>
      <translation>In das Hauptfenster integrieren</translation>
    </message>
    <message>
      <location filename="../qml/about/PQAbout.qml" line="272"/>
      <location filename="../qml/advancedsort/PQAdvancedSort.qml" line="297"/>
      <location filename="../qml/chromecast/PQChromecast.qml" line="291"/>
      <location filename="../qml/filedialog/PQFileDialog.qml" line="232"/>
      <location filename="../qml/filemanagement/PQDelete.qml" line="232"/>
      <location filename="../qml/filemanagement/PQRename.qml" line="215"/>
      <location filename="../qml/filemanagement/PQSaveAs.qml" line="347"/>
      <location filename="../qml/filter/PQFilter.qml" line="363"/>
      <location filename="../qml/histogram/PQHistogram.qml" line="300"/>
      <location filename="../qml/menumeta/PQMainMenu.qml" line="427"/>
      <location filename="../qml/menumeta/PQMetaData.qml" line="306"/>
      <location filename="../qml/scale/PQScale.qml" line="331"/>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="373"/>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="534"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="405"/>
      <source>Move to its own window</source>
      <extracomment>Tooltip of small button to show an element in its own window (i.e., not merged into main interface)</extracomment>
      <translation>In eigenem Fenster anzeigen</translation>
    </message>
  </context>
  <context>
    <name>quickinfo</name>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="122"/>
      <source>Click here to show main menu</source>
      <translation>Hier klicken, um das Hauptmenü anzuzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="151"/>
      <source>Click here to enter fullscreen mode</source>
      <translation>Hier klicken, um den Vollbildmodus zu aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="152"/>
      <source>Click here to exit fullscreen mode</source>
      <translation>Hier klicken, um den Vollbildmodus zu beenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="190"/>
      <source>Click here to close PhotoQt</source>
      <translation>Hier klicken, um die PhotoQt zu schließen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="213"/>
      <source>Show counter</source>
      <translation>Zähler anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="214"/>
      <source>Hide counter</source>
      <translation>Zähler ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="216"/>
      <source>Show file path</source>
      <translation>Dateipfad anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="217"/>
      <source>Hide file path</source>
      <translation>Dateipfad ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="219"/>
      <source>Show file name</source>
      <translation>Dateinamen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="220"/>
      <source>Hide file name</source>
      <translation>Dateinamen ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="222"/>
      <source>Show zoom level</source>
      <translation>Zoomfaktor anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="223"/>
      <source>Hide zoom level</source>
      <translation>Zoomfaktor ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="712"/>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="388"/>
      <source>Click here to enter viewer mode</source>
      <translation>Hier klicken, um den Anschaumodus zu aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQImage.qml" line="731"/>
      <source>Hide central &apos;viewer mode&apos; button</source>
      <translation>Verstecke den zentralen Knopf für den &apos;Anschaumodus&apos;</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="238"/>
      <source>Some information about the current image and directory</source>
      <translation>Einige Informationen zum aktuellen Bild und Verzeichnis</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="289"/>
      <source>Connected to:</source>
      <extracomment>This is followed by the name of the Chromecast streaming device currently connected to</extracomment>
      <translation>Verbunden mit:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="387"/>
      <source>Click here to exit viewer mode</source>
      <translation>Hier klicken, um den Anschaumodus zu beenden</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="342"/>
      <source>Filter:</source>
      <translation>Filter:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="225"/>
      <source>Show window buttons</source>
      <translation>Fensterknöpfe anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQWindowButtons.qml" line="226"/>
      <source>Hide window buttons</source>
      <translation>Fensterknöpfe verstecken</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQStatusInfo.qml" line="329"/>
      <source>Click to remove filter</source>
      <translation>Klicke, um den Filter zu entfernen</translation>
    </message>
  </context>
  <context>
    <name>scale</name>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="84"/>
      <location filename="../qml/scale/PQScalePopout.qml" line="34"/>
      <source>Scale file</source>
      <extracomment>Window title</extracomment>
      <translation>Datei skalieren</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="94"/>
      <source>An error occured, file could not be scaled!</source>
      <translation>Ein Fehler ist aufgetreten, die Datei konnte nicht skaliert werden!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="104"/>
      <source>This file format cannot (yet) be scaled with PhotoQt!</source>
      <translation>Dieses Dateiformat kann (noch) nicht mit PhotoQt skaliert werden!</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="115"/>
      <source>New width x height:</source>
      <translation>Neue Breite x Höhe:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="188"/>
      <source>Preserve aspect ratio</source>
      <extracomment>The aspect ratio refers to the ratio of the width to the height of the image, e.g., 16:9 for most movies</extracomment>
      <translation>Seitenverhältnis beibehalten</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="199"/>
      <source>Quality:</source>
      <extracomment>This refers to the quality to be used to scale the image</extracomment>
      <translation>Qualität:</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="232"/>
      <location filename="../qml/scale/PQScale.qml" line="302"/>
      <source>Scale (create new file)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Skalieren (neue Datei erstellen)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="260"/>
      <location filename="../qml/scale/PQScale.qml" line="303"/>
      <source>Scale (change file in place)</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Skalieren (Original-Datei ändern)</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="304"/>
      <source>De-/Increase width and height by 10%</source>
      <translation>Breite und Höhe um 10% erhöhen/erniedrigen</translation>
    </message>
    <message>
      <location filename="../qml/scale/PQScale.qml" line="305"/>
      <source>In-/Decrease quality by 5%</source>
      <translation>Qualität um 5% erhöhen/erniedrigen</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingmanipulation.cpp" line="233"/>
      <source>Select new file</source>
      <translation>Neue Datei auswählen</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager</name>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="100"/>
      <source>interface</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Benutzeroberfläche</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="101"/>
      <source>Tab to control interface settings</source>
      <translation>Tab um die Einstellungen der Oberfläche zu kontrollieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="103"/>
      <source>image view</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Bildansicht</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="104"/>
      <source>Tab to control how images are viewed</source>
      <translation>Tab um zu kontrollieren, wie Bilder angezeigt werden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="106"/>
      <source>thumbnails</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Miniaturbilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="107"/>
      <source>Tab to control the look and behaviour of thumbnails</source>
      <translation>Tab um das Aussehen und das Verhalten der Miniaturbilder zu kontrollieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="109"/>
      <source>metadata</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="110"/>
      <source>Tab to control metadata settings</source>
      <translation>Tab um die Einstellungen der Metadaten zu kontrollieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="112"/>
      <source>file types</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Dateitypen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="113"/>
      <source>Tab to control which file types PhotoQt should recognize</source>
      <translation>Tab um zu kontrollieren, welche Dateitypen PhotoQt erkennen soll</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="115"/>
      <source>shortcuts</source>
      <extracomment>settings manager tab title</extracomment>
      <translation>Kurzbefehle</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="116"/>
      <source>Tab to control which shortcuts are set</source>
      <translation>Tab um die Kurzbefehle zu kontrollieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="224"/>
      <source>advanced</source>
      <extracomment>Written on button in setting manager. A click on this button opens a menu with some advanced actions.</extracomment>
      <translation>erweitert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="228"/>
      <source>restore defaults</source>
      <translation>Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="229"/>
      <source>import settings</source>
      <translation>Einstellungen importieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="230"/>
      <source>export settings</source>
      <translation>Einstellungen exportieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>disable expert mode</source>
      <translation>Expertenmodus ausschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="231"/>
      <source>enable expert mode</source>
      <translation>Expertenmodus einschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="272"/>
      <source>Import of %1. This will replace your current settings with the ones stored in the backup.</source>
      <translation>Importieren von %1. Dies wird deine aktuellen Einstellungen durch die im Backup gespeicherten ersetzen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="273"/>
      <source>Do you want to continue?</source>
      <translation>Möchtest du fortfahren?</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="325"/>
      <source>Save changes and exit</source>
      <translation>Änderungen speichern und schließen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManager.qml" line="338"/>
      <source>Exit and discard changes</source>
      <translation>Schließen und Änderungen verwerfen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="179"/>
      <source>Rename File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="181"/>
      <source>Delete File</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="76"/>
      <source>Filetype settings</source>
      <translation>Dateityp-Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>These settings govern which file types PhotoQt should recognize and open.</source>
      <translation>Diese Einstellungen regeln welche Dateitypen PhotoQt erkennen und öffnen soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabFileTypes.qml" line="90"/>
      <source>Not all file types might be available, depending on your setup and what library support was enabled at compile time</source>
      <translation>Abhängig von deinem Setup und abhängig von welche Bibliotheksunterstützung beim Kompilieren aktiviert wurde sind möglicherweise nicht alle Dateitypen verfügbar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="84"/>
      <source>Image view settings</source>
      <translation>Bildeinstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <source>These settings affect the viewing of images, how they are shown, in what order, how large a cache to use, etc.</source>
      <translation>Diese Einstellungen beeinflussen die Ansicht von Bildern, wie sie angezeigt werden, in welcher Reihenfolge, wie groß der Zwischenspeicher sein soll, usw.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabImageView.qml" line="98"/>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>Some settings are only shown in expert mode.</source>
      <translation>Einige Einstellungen werden nur im Expertenmodus angezeigt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="86"/>
      <source>Interface settings</source>
      <translation>Einstellungen der Oberfläche</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabInterface.qml" line="100"/>
      <source>These settings affect the interface in general, how the application looks like and behaves. This includes the background, some of the labels in the main view, which elements are to be shown in their own window, and others.</source>
      <translation>Diese Einstellungen beeinflussen die Benutzeroberfläche im Allgemeinen, wie die Anwendung aussieht und sich verhält. Dazu gehören der Hintergrund, einige der Beschriftungen in der Hauptansicht, welche Elemente in ihrem eigenen Fenster angezeigt werden sollen, und andere.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="85"/>
      <source>Metadata settings</source>
      <translation>Einstellungen für die Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabMetadata.qml" line="99"/>
      <source>These settings affect the metadata element, what information it should show and some of its behavior.</source>
      <translation>Diese Einstellungen beeinflussen das Element für die Metadaten, welche Informationen es zeigen soll und sein Verhalten.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="77"/>
      <source>Shortcuts</source>
      <translation>Kurzbefehle</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="101"/>
      <source>Navigation</source>
      <extracomment>A shortcuts category: navigation</extracomment>
      <translation>Navigation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="107"/>
      <source>Filter images in folder</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bilder im Ordner filtern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="109"/>
      <source>Next image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Nächstes Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="111"/>
      <source>Previous image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Vorheriges Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="113"/>
      <source>Load a random image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Ein zufälliges Bild laden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="117"/>
      <source>Go to first image</source>
      <extracomment>Name of shortcut action Name of shortcut action</extracomment>
      <translation>Gehe zum ersten Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="119"/>
      <source>Go to last image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Gehe zum letzten Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="121"/>
      <source>Enter viewer mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Anschaumodus aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="125"/>
      <source>Close window (hides to system tray if enabled)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Fenster schließen (versteckt in der Systemleiste wenn aktiviert)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="127"/>
      <source>Quit PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>PhotoQt beenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="135"/>
      <source>Image</source>
      <extracomment>A shortcuts category: image manipulation</extracomment>
      <translation>Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="139"/>
      <source>Zoom In</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Hineinzoomen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="141"/>
      <source>Zoom Out</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Herauszoomen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="143"/>
      <source>Zoom to Actual Size</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zoom auf tatsächliche Größe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="145"/>
      <source>Reset Zoom</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zoom zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="147"/>
      <source>Toggle &apos;Fit in window&apos;</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>&apos;An Fenstergröße anpassen&apos; umschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="149"/>
      <source>Rotate Right</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Nach Rechts drehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="151"/>
      <source>Rotate Left</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Nach Links drehen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="153"/>
      <source>Reset Rotation</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Drehung zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="155"/>
      <source>Flip Horizontally</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Horizontal spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="157"/>
      <source>Flip Vertically</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Vertikal spiegeln</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="159"/>
      <source>Scale Image</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild skalieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="161"/>
      <source>Play/Pause animation/video</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Animation/Video abspielen/pausieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="163"/>
      <source>Hide/Show face tags (stored in metadata)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Markierungen für Gesichter anzeigen/verstecken (in den Metadaten gespeichert)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="167"/>
      <source>Advanced image sort (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Erweiterte Bildsortierung (Schnellstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="215"/>
      <source>Start Slideshow (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Diaschau starten (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="245"/>
      <source>filename including path</source>
      <translation>Dateiname inklusive Pfad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="246"/>
      <source>filename without path</source>
      <translation>Dateiname ohne Pfad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="247"/>
      <source>directory containing file</source>
      <translation>Verzeichnis der Datei</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="175"/>
      <source>File</source>
      <extracomment>A shortcuts category: file management</extracomment>
      <translation>Datei</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="123"/>
      <source>Show floating navigation buttons</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Schwebende Knöpfe zur Navigation anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="183"/>
      <source>Delete File (without confirmation)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei löschen (ohne Bestätigung)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="185"/>
      <source>Copy File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei an einen neuen Ort kopieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="187"/>
      <source>Move File to a New Location</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei an einen neuen Ort verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="189"/>
      <source>Copy Image to Clipboard</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild in Zwischenablage kopieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="193"/>
      <source>Print current photo</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Aktuelles Foto drucken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="201"/>
      <source>Other</source>
      <extracomment>A shortcuts category: other functions</extracomment>
      <translation>Andere</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="205"/>
      <source>Hide/Show main menu</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Hauptmenü anzeigen/verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="207"/>
      <source>Hide/Show metadata</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Metadaten verstecken/anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="209"/>
      <source>Keep metadata opened</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Metadaten offen halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="211"/>
      <source>Hide/Show thumbnails</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Miniaturbilder anzeigen/verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="213"/>
      <source>Show Settings</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Einstellungen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="217"/>
      <source>Start Slideshow (Quickstart)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Diaschau starten (Schnellstart)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="219"/>
      <source>About PhotoQt</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Über PhotoQt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="221"/>
      <source>Set as Wallpaper</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Als Hintergrundbild festlegen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="223"/>
      <source>Show Histogram</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zeige Histogramm</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="225"/>
      <source>Upload to imgur.com (anonymously)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Auf imgur.com hochladen (anonym)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="227"/>
      <source>Upload to imgur.com user account</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Zu Benutzerkonto auf imgur.com hochladen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="229"/>
      <source>Stream content to Chromecast device</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Inhalt auf Chromecast-Gerät übertragen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="231"/>
      <source>Show log/debug messages</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Protokoll- und Debugmeldungen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="233"/>
      <source>Toggle fullscreen mode</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Vollbildmodus umschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="243"/>
      <source>External</source>
      <extracomment>A shortcuts category: external shortcuts</extracomment>
      <translation>Extern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="85"/>
      <source>Thumbnails settings</source>
      <translation>Miniaturbilder-Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabThumbnails.qml" line="99"/>
      <source>These settings affect the thumbnails shown, by default, along the bottom edge of the screen. This includes their look, behavior, and the user&apos;s interaction with them.</source>
      <translation>Diese Einstellungen beeinflussen die Miniaturbilder, die standardmäßig am unteren Rand des Bildschirms angezeigt werden. Dies beinhaltet ihr Aussehen, ihr Verhalten und die Interaktion des Benutzers&apos;s mit ihnen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQSettingsManagerPopout.qml" line="34"/>
      <source>Settings Manager</source>
      <extracomment>Window title</extracomment>
      <translation>Einstellungsmanager</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>Here the shortcuts can be managed. Below you can add a new shortcut for any one of the available actions, both key combinations and mouse gestures are supported.</source>
      <translation>Hier können die Kurzbefehle verwaltet werden. Unten können neue Kurzbefehle für die verfügbaren Aktionen hinzugefügt werden. Es werden sowohl Tastenkombinationen als auch Mausgesten unterstützt.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="91"/>
      <source>You can also set the same shortcut for multiple actions or multiple times for the same action. All actions for a shortcut will be executed sequentially, allowing a lot more flexibility in using PhotoQt.</source>
      <translation>Der gleiche Kurzbefehl kann für mehrere Aktionen oder auch mehrmals für die gleiche Aktion gesetzt werden. Alle Aktionen für einen Kurzbefehl werden nacheinander ausgeführt, was viel mehr Flexibilität in der Nutzung PhotoQt's ermöglicht.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="105"/>
      <source>Open file (browse images)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Datei öffnen (Bilder durchsuchen)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="165"/>
      <source>Advanced image sort (Setup)</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Erweiterte Bildsortierung (Setup)</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/PQTabShortcuts.qml" line="191"/>
      <source>Save image in another format</source>
      <extracomment>Name of shortcut action</extracomment>
      <translation>Bild in einem anderen Format speichern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="72"/>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="128"/>
      <source>Restore defaults</source>
      <extracomment>As in &apos;restore the default settings and/or file formats and/or shortcuts&apos;. Please keep short!</extracomment>
      <translation>Standardeinstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="83"/>
      <source>Here you can restore the default configuration of PhotoQt. You can choose to restore any combination of the following three categories.</source>
      <translation>Hier können die Standardeinstellungen von PhotoQt wiederhergestellt werden. Es kann eine beliebige Kombination der drei folgenden Kategorien ausgewählt werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="96"/>
      <source>Restore default settings</source>
      <translation>Einstellungen auf Standard zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="103"/>
      <source>Restore default file formats</source>
      <translation>Dateiformate auf Standard zurücksetzen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/PQRestoreDefaults.qml" line="109"/>
      <source>Restore default shortcuts</source>
      <translation>Kurzbefehle auf Standard zurücksetzen</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_filetypes</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="32"/>
      <source>These are some additional settings for opening archives.</source>
      <translation>Dies sind einige zusätzliche Einstellungen für das Öffnen von Archiven.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQLibArchive.qml" line="45"/>
      <source>use external tool: unrar</source>
      <extracomment>used as label for checkbox</extracomment>
      <translation>externes Werkzeug verwenden: unrar</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="32"/>
      <source>These are some additional settings for showing PDFs.</source>
      <translation>Dies sind einige zusätzliche Einstellungen für das Anzeigen von PDFs.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="44"/>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQPoppler.qml" line="55"/>
      <source>Quality:</source>
      <extracomment>the quality setting to be used when loading PDFs</extracomment>
      <translation>Qualität:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="32"/>
      <source>These are some additional settings for playing videos.</source>
      <translation>Dies sind einige zusätzliche Einstellungen für das Abspielen von Videos.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="45"/>
      <source>Autoplay</source>
      <extracomment>Used as setting for video files (i.e., autoplay videos)</extracomment>
      <translation>Autoplay</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="51"/>
      <source>Loop</source>
      <extracomment>Used as setting for video files (i.e., loop videos)</extracomment>
      <translation>Wiederholen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="58"/>
      <source>Prefer libmpv</source>
      <extracomment>Used as setting for video files</extracomment>
      <translation>Bevorzuge libmpv</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="59"/>
      <source>libmpv can offer a more comprehensive video support but tends to be slightly slower to load.</source>
      <translation>libmpv kann eine umfassendere Videounterstützung anbieten, ist aber in der Regel etwas langsamer beim Laden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQVideo.qml" line="64"/>
      <source>Select tool for creating video thumbnails</source>
      <extracomment>Tooltip shown for combobox for selectiong video thumbnailer</extracomment>
      <translation>Werkzeug zum Erstellen von Vorschaubildern für Videos auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="55"/>
      <source>images</source>
      <extracomment>This is a category of files PhotoQt can recognize: any image format</extracomment>
      <translation>Bilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="57"/>
      <source>compressed files</source>
      <extracomment>This is a category of files PhotoQt can recognize: compressed files like zip, tar, cbr, 7z, etc.</extracomment>
      <translation>Komprimierte Dateien</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="59"/>
      <source>documents</source>
      <extracomment>This is a category of files PhotoQt can recognize: documents like pdf, txt, etc.</extracomment>
      <translation>Dokumente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="61"/>
      <source>videos</source>
      <extracomment>This is a type of category of files PhotoQt can recognize: videos like mp4, avi, etc.</extracomment>
      <translation>Videos</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="67"/>
      <source>Enable</source>
      <extracomment>As in: &quot;Enable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="73"/>
      <source>Disable</source>
      <extracomment>As in: &quot;Disable all formats in the seleted category of file types&quot;</extracomment>
      <translation>Deaktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="99"/>
      <source>Enable everything</source>
      <extracomment>As in &quot;Enable every single file format PhotoQt can open in any category&quot;</extracomment>
      <translation>Alles aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="117"/>
      <source>Currently there are %1 file formats enabled</source>
      <extracomment>The %1 will be replaced with the number of file formats, please don&apos;t forget to add it.</extracomment>
      <translation>Momentan sind %1 Dateiformate aktiviert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="145"/>
      <source>Search by description or file ending</source>
      <translation>Suche nach Beschreibung oder Dateiendung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="151"/>
      <source>Search by image library or category</source>
      <translation>Suche nach Bildbibliothek oder Kategorie</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/filetypes/PQFileTypes.qml" line="240"/>
      <source>File endings:</source>
      <translation>Dateiendungen:</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_imageview</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="31"/>
      <source>animation</source>
      <extracomment>A settings title referring to the in/out animation of images</extracomment>
      <translation>Animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="33"/>
      <source>What type of animation to show, and how fast.</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Welche Art von Animation zu zeigen, und wie schnell.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="44"/>
      <source>type of animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Art der Animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="47"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Deckkraft</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="49"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>entlang der x-Achse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="51"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>entlang der y-Achse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="53"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Drehung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="55"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Explosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="57"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Implosion</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="59"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>zufällig auswählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="76"/>
      <source>no animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>keine Animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQAnimation.qml" line="91"/>
      <source>long animation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>lange Animation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="30"/>
      <source>fit in window</source>
      <extracomment>A settings title referring to whether to fit images in window</extracomment>
      <translation>An Fenstergröße anpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="31"/>
      <source>Zoom smaller images to fill the full window width and/or height.</source>
      <translation>Vergrößere kleinere Bilder, um die volle Fensterbreite und/oder Fensterhöhe zu füllen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQFitInWindow.qml" line="36"/>
      <source>fit smaller images in window</source>
      <translation>kleinere Bilder im Fenster anpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="30"/>
      <source>interpolation</source>
      <extracomment>A settings title referring to the type of interpolation to use for small images</extracomment>
      <translation>Interpolation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="31"/>
      <source>PhotoQt tries to improve the rendering of images that are shown much larger than they are (i.e., zoomed in a lot). For very tiny images that are zoomed in quite a lot, this can result in the loss of too much information in the image. Thus a threshold can be defined here, images that are smaller than this threshold are shown exactly as they are without any smoothing or other attempts to improve them.</source>
      <translation>PhotoQt versucht die Darstellung von Bildern zu verbessern, die viel größer dargestellt werden als sie sind (z.B. bei hohem Zoom). Für sehr kleine Bilder, die stark vergrößert dargestellt werden, kann dies zu einem Verlust von zu viel Informationen führen. Daher kann hier ein Schwellenwert definiert werden: Bilder, die kleiner als dieser Schwellenwert sind, werden unverändert angezeigt, ohne jegliche Versuche, ihre Darstellung zu verbessern.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="42"/>
      <source>Do not use any interpolation algorithm for very small images</source>
      <extracomment>A type of interpolation to use for small images</extracomment>
      <translation>Sehr kleine Bilder nicht interpolieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQInterpolation.qml" line="53"/>
      <source>threshold:</source>
      <extracomment>The threshold (in pixels) at which to switch interpolation algorithm</extracomment>
      <translation>Schwellenwert:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="30"/>
      <source>remember per session</source>
      <extracomment>A settings title</extracomment>
      <translation>pro Sitzung speichern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="31"/>
      <source>By default, PhotoQt resets the zoom, rotation, flipping/mirroring and position when switching to a different image. For certain tasks, for example for comparing two images, it can be helpful to keep these properties.</source>
      <translation>Standardmäßig setzt PhotoQt beim Wechseln auf ein anderes Bild den Zoom, die Drehung, das Spiegeln und die Position zurück. Bei bestimmten Aufgaben, zum Beispiel beim Vergleich zweier Bilder, kann es hilfreich sein, diese Eigenschaften beizubehalten.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQKeep.qml" line="37"/>
      <source>remember zoom, rotation, flip, position</source>
      <translation>an Zoom, Drehen, spiegeln, Position erinnern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="30"/>
      <source>looping</source>
      <extracomment>A settings title for looping through images in folder</extracomment>
      <translation>Wiederholen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="31"/>
      <source>What to do when the end of a folder has been reached: stop or loop back to first image in folder.</source>
      <translation>Was zu tun ist, wenn das Ende eines Ordners erreicht wurde: Stoppen oder zurück zum ersten Bild im Ordner.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQLoop.qml" line="36"/>
      <source>loop through images in folder</source>
      <translation>Bilder im Ordner wiederholen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="30"/>
      <source>margin</source>
      <extracomment>A settings title about the margin around the main image</extracomment>
      <translation>Rand</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="31"/>
      <source>How much space to show between the main image and the application border.</source>
      <translation>Wie viel Abstand zwischen dem Hauptbild und dem Anwendungsrahmen angezeigt werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQMargin.qml" line="44"/>
      <source>none</source>
      <extracomment>As in: no margin between the main image and the window edges</extracomment>
      <translation>kein</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="30"/>
      <source>pixmap cache</source>
      <extracomment>A settings title</extracomment>
      <translation>Zwischenspeicher für Bilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQPixmapCache.qml" line="31"/>
      <source>Size of runtime cache for fully loaded images. This cache is cleared when the application quits.</source>
      <translation>Größe des Laufzeit-Zwischenspeichers für geladene Bilder. Dieser Zwischenspeicher wird gelöscht, wenn die Anwendung beendet wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="31"/>
      <source>sort images by</source>
      <extracomment>A settings title</extracomment>
      <translation>sortiere Bilder nach</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="32"/>
      <source>Sort all images in a folder by the set property.</source>
      <translation>Sortiere alle Bilder in einem Ordner nach der festgelegten Eigenschaft.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="43"/>
      <source>natural name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>natürlicher Name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="45"/>
      <source>name</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Name</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="47"/>
      <source>time</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Zeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="49"/>
      <source>size</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Größe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="51"/>
      <source>type</source>
      <extracomment>A criteria for sorting images</extracomment>
      <translation>Typ</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="58"/>
      <source>ascending</source>
      <extracomment>Sort images in ascending order</extracomment>
      <translation>aufsteigend</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQSort.qml" line="65"/>
      <source>descending</source>
      <extracomment>Sort images in descending order</extracomment>
      <translation>absteigend</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="30"/>
      <source>transparency marker</source>
      <extracomment>A settings title</extracomment>
      <translation>Markierung der Transparenz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="31"/>
      <source>Show checkerboard pattern behind transparent areas of (half-)transparent images.</source>
      <translation>Schachbrettmuster hinter transparenten Bereichen (halb-)transparenter Bilder anzeigen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQTransparencyMarker.qml" line="37"/>
      <source>show checkerboard pattern</source>
      <extracomment>Setting for how to display images that have transparent areas, whether to show checkerboard pattern in that area or not</extracomment>
      <translation>Schachbrettmuster zeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="30"/>
      <source>zoom speed</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>Zoomgeschwindigkeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="31"/>
      <source>Images are zoomed at a relative speed as specified by this percentage. A higher value means faster zoom.</source>
      <translation>Bilder werden mit einer relativen Geschwindigkeit gezoomt, wie in diesem Prozentsatz angegeben. Ein höherer Wert bedeutet schnelleren Zoom.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="43"/>
      <source>super slow</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation>super langsam</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomSpeed.qml" line="58"/>
      <source>very fast</source>
      <extracomment>This refers to the zoom speed, the zoom here is the zoom of the main image</extracomment>
      <translation>sehr schnell</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="31"/>
      <source>zoom to/from</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>Zoom zu/von</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="32"/>
      <source>This controls whether the image is zoomed to/from the mouse position or to/from the image center. Note that this only applies when zooming by mouse, zooming by keyboard shortcut always zooms to/from the image center.</source>
      <translation>Dies legt fest, ob das Bild von/nach der Mausposition oder von/nach der Bildmitte gezoomt wird. Beachte, dass dies nur beim Zoomen mit der Maus gilt, das Zoomen durch Tastaturkürzel wird immer von/nach der Bildmitte vorgenommen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="42"/>
      <source>mouse position</source>
      <translation>Position des Mauszeigers</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomToFrom.qml" line="47"/>
      <source>image center</source>
      <translation>Mitte des Bildes</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="30"/>
      <source>zoom min/max</source>
      <extracomment>A settings title, the zoom here is the zoom of the main image</extracomment>
      <translation>Min/Max Zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>This defines the minimum and/or maximum zoom level for an image.</source>
      <translation>Dies bestimmt wie weit ein Bild heraus-/hineingezoomt werden kann.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="31"/>
      <source>Note that the maximum zoom level is the absolute zoom level, the minimum zoom level is relative to the default zoom level (the zoom level when the image is first loaded).</source>
      <translation>Beachte, dass die maximale Vergrößerung die absolute Zoomstufe ist, jedoch die minimale Vergrößerung relativ zum Standardzoom (die Größe beim anfänglichem Laden des Bildes) anzugeben ist.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="52"/>
      <source>minimum zoom:</source>
      <translation>minimaler Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/imageview/PQZoomMinMax.qml" line="91"/>
      <source>maximum zoom:</source>
      <translation>maximaler Zoom:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="30"/>
      <source>Double Click Threshold</source>
      <extracomment>A settings title</extracomment>
      <translation>Schwellenwert für den Doppelklick</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQDoubleClick.qml" line="31"/>
      <source>Two clicks within the specified interval are interpreted as a double click. Note that too high a value will cause noticable delays in reacting to single clicks.</source>
      <translation>Zwei Klicks innerhalb des angegebenen Intervalls werden als Doppelklick interpretiert. Beachte, dass zu hohe Werte zu spürbaren Verzögerungen bei der Reaktion auf einzelne Klicks führen können.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_interface</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="32"/>
      <source>background</source>
      <extracomment>A settings title referring to the background of PhotoQt (behind any image/element)</extracomment>
      <translation>Hintergrund</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="34"/>
      <source>What type of background is to be shown.</source>
      <extracomment>The background here refers to the area behind the main image and any element in PhotoQt, the very back.</extracomment>
      <translation>Welche Art von Hintergrund angezeigt werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="45"/>
      <source>(half-)transparent background</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>(halb-)transparenter Hintergrund</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="47"/>
      <source>faked transparency</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>falsche Transparenz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="49"/>
      <source>custom background image</source>
      <extracomment>How the background of PhotoQt should be</extracomment>
      <translation>benutzerdefiniertes Hintergrundbild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="76"/>
      <source>Click to select an image</source>
      <extracomment>Tooltip for a mouse area, a click on which opens a file dialog for selecting an image</extracomment>
      <translation>Klicken um das Bild auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="87"/>
      <source>scale to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Skaliert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="89"/>
      <source>scale and crop to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Skaliert und beschnitten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="91"/>
      <source>stretch to fit</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>An Fenster anpassen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="93"/>
      <source>center image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Zentriert</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="95"/>
      <source>tile image</source>
      <extracomment>If an image is set as background of PhotoQt this is one way it can be handled.</extracomment>
      <translation>Gekachelt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="201"/>
      <source>All Images</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: All images supported by PhotoQt.</extracomment>
      <translation>Alle Bilder</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQBackground.qml" line="216"/>
      <source>Video</source>
      <extracomment>This is a category in a file dialog for selecting images used as in: Video files supported by PhotoQt.</extracomment>
      <translation>Video</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="31"/>
      <source>empty area around image</source>
      <extracomment>A settings title</extracomment>
      <translation>leerer Bereich um Bild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="32"/>
      <source>How to handle clicks on empty area around images.</source>
      <translation>Wie mit Klicks auf den leeren Bereich um die Bilder umzugehen ist.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="40"/>
      <source>close on click</source>
      <extracomment>Used as in: Close PhotoQt on click on empty area around main image</extracomment>
      <translation>beim Klicken schließen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="41"/>
      <source>Close PhotoQt when click occurred on empty area around image</source>
      <translation>PhotoQt schließen, wenn auf einen leeren Bereich um das Bild geklickt wurde</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="52"/>
      <source>navigate on click</source>
      <extracomment>Used as in: Navigate in folder on click on empty area around main image</extracomment>
      <translation>navigieren bei Klick</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="53"/>
      <source>Go to next/previous image if click occurred in left/right half of window</source>
      <translation>Gehe zum nächsten/vorherigen Bild, wenn im linken/rechten Teil des Fensters geklickt wurde</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="63"/>
      <source>toggle window decoration</source>
      <translation>Fensterdekoration umschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQClickOnEmpty.qml" line="64"/>
      <source>Toggle window decoration</source>
      <translation>Fensterdekoration umschalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="30"/>
      <source>size of &apos;hot edge&apos;</source>
      <extracomment>A settings title. The hot edge refers to the area along the edges of PhotoQt where the mouse cursor triggers an action (e.g., showing the thumbnails or the main menu)</extracomment>
      <translation>Größe der &apos;heißen Kante&apos;</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="31"/>
      <source>Adjusts the sensitivity of the edges for showing elements like the metadata and main menu elements.</source>
      <translation>Passt die Empfindlichkeit der Kanten an, mit welcher die Elemente für z. B. die Metadaten und das Hauptmenü anzuzeigen sind.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="40"/>
      <source>small</source>
      <extracomment>used as in: small area</extracomment>
      <translation>klein</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQHotEdgeWidth.qml" line="57"/>
      <source>large</source>
      <extracomment>used as in: large area</extracomment>
      <translation>groß</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="33"/>
      <source>language</source>
      <extracomment>A settings title.</extracomment>
      <translation>Sprache</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQLanguage.qml" line="34"/>
      <source>Change the language of the application.</source>
      <translation>Ändere die Sprache der Anwendung.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="30"/>
      <source>mouse wheel sensitivity</source>
      <extracomment>A settings title.</extracomment>
      <translation>Mausradempfindlichkeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="31"/>
      <source>How sensitive the mouse wheel is for shortcuts, etc.</source>
      <translation>Wie empfindlich das Mausrad ist für Kurzbefehle und anderes.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="58"/>
      <source>not sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation>unempfindlich</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQMouseWheel.qml" line="42"/>
      <source>very sensitive</source>
      <extracomment>The sensitivity here refers to the sensitivity of the mouse wheel</extracomment>
      <translation>sehr empfindlich</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="32"/>
      <source>overlay color</source>
      <extracomment>A settings title.</extracomment>
      <translation>Überlagerungsfarbe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="33"/>
      <source>This is the color that is shown in the background on top of any background image/etc.</source>
      <translation>Dies ist die Farbe, die im Hintergrund über dem Hintergrundbild/... angezeigt wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="77"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="148"/>
      <source>click to change color</source>
      <translation>klicken, um die Farbe zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="122"/>
      <source>fullscreen mode</source>
      <translation>Vollbildmodus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="186"/>
      <source>use same color in window and fullscreen mode</source>
      <translation>dieselbe Farbe im Fenster- und Vollbildmodus verwenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="195"/>
      <source>please choose a color</source>
      <translation>Bitte eine Farbe wählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="31"/>
      <source>pop out elements</source>
      <extracomment>A settings title. The popping out that is talked about here refers to the possibility of showing any element in its own window (i.e., popped out).</extracomment>
      <translation>Pop-out-Elemente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="32"/>
      <source>Here you can choose for most elements whether they are to be shown integrated into the main window or in their own, separate window.</source>
      <translation>Hier kann für die meisten Elemente ausgewählt werden, ob sie in das Hauptfenster integriert oder in einem eigenen, separaten Fenster angezeigt werden sollen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>File dialog</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Dateidialog</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="35"/>
      <source>keep open</source>
      <translation>offen halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="37"/>
      <source>Settings Manager</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Einstellungsmanager</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="39"/>
      <source>Main Menu</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Hauptmenü</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="41"/>
      <source>Metadata</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="43"/>
      <source>Histogram</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Histogramm</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="45"/>
      <source>Scale</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Skalieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="47"/>
      <source>Slideshow Settings</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Diaschau - Einstellungen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="49"/>
      <source>Slideshow Controls</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Diaschau - Steuerelemente</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="51"/>
      <source>Rename File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Datei umbenennen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="53"/>
      <source>Delete File</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Datei löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="55"/>
      <source>Save File As</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Datei speichern als</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="57"/>
      <source>About</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Über</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="59"/>
      <source>Imgur</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Imgur</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="61"/>
      <source>Wallpaper</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Hintergrundbild</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="63"/>
      <source>Filter</source>
      <extracomment>Noun, not a verb. Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Filter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopout.qml" line="65"/>
      <source>Advanced Image Sort</source>
      <extracomment>Used as identifying name for one of the elements in the interface</extracomment>
      <translation>Erweiterte Bildsortierung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="31"/>
      <source>status information</source>
      <extracomment>A settings title.</extracomment>
      <translation>Statusinformationen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="32"/>
      <source>The status information shows some basic data about the current folder and photo in the top left corner of the window. The items can be reordered using drag-and-drop.</source>
      <translation>Die Statusinformationen zeigen einige grundlegende Daten zum aktuellen Ordner und Foto in der oberen linken Ecke des Fensters. Die Reihenfolge der Einträge kann durch Klicken und Ziehen geändert werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="41"/>
      <source>Show status information</source>
      <translation>Zeige Statusinformationen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="72"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="204"/>
      <source>counter</source>
      <extracomment>Please keep short! The counter shows where we are in the folder.</extracomment>
      <translation>Zähler</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="76"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="208"/>
      <source>filepath</source>
      <extracomment>Please keep short!</extracomment>
      <translation>Dateipfad</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="78"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="210"/>
      <source>resolution</source>
      <extracomment>Please keep short! This is the image resolution.</extracomment>
      <translation>Auflösung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="80"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="212"/>
      <source>zoom</source>
      <extracomment>Please keep short! This is the current zoom level.</extracomment>
      <translation>Zoom</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="82"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="214"/>
      <source>rotation</source>
      <extracomment>Please keep short! This is the rotation of the current image</extracomment>
      <translation>Drehung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="84"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="216"/>
      <source>filesize</source>
      <extracomment>Please keep short! This is the filesize of the current image.</extracomment>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="231"/>
      <source>Add</source>
      <extracomment>This is written on a button that is used to add a selected block to the status info section.</extracomment>
      <translation>Hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="242"/>
      <source>Font size:</source>
      <translation>Schriftgröße:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="74"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQStatusInfo.qml" line="206"/>
      <source>filename</source>
      <extracomment>Please keep short!</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="31"/>
      <source>window buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation>Fensterknöpfe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="32"/>
      <source>PhotoQt can show some integrated window buttons at the top right corner of the screen.</source>
      <translation>PhotoQt kann einige integrierte Fenstertasten in der oberen rechten Ecke des Fensters anzeigen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="44"/>
      <source>show integrated window buttons</source>
      <translation>integrierte Fenstertasten anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="50"/>
      <source>always show &apos;x&apos;</source>
      <translation>immer &apos;x&apos; anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowButtons.qml" line="61"/>
      <source>size of window buttons</source>
      <extracomment>the size of the window buttons (the buttons shown in the top right corner of the window)</extracomment>
      <translation>Größe der Fensterknöpfe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="30"/>
      <source>remember last image</source>
      <extracomment>A settings title.</extracomment>
      <translation>letztes Bild merken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="31"/>
      <source>At startup the image loaded at the end of the last session can be automatically reloaded.</source>
      <translation>Beim Start kann das am Ende der letzten Sitzung geladene Bild automatisch neu geladen werden.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQStartupLoadLast.qml" line="35"/>
      <source>re-open last loaded image at startup</source>
      <translation>zuletzt geladenes Bild beim Start erneut öffnen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="30"/>
      <source>tray icon</source>
      <extracomment>A settings title.</extracomment>
      <translation>Symbol in der Systemleiste</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="31"/>
      <source>If a tray icon is to be shown and, if shown, whether to hide it or not.</source>
      <translation>Falls ein Symbol in der Systemleiste angezeigt werden soll, und wenn ja, ob die Anwendung sich dahin verstecken soll beim Schließen oder nicht.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="36"/>
      <source>no tray icon</source>
      <translation>kein Symbol in der Systemleiste</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="37"/>
      <source>hide to tray icon</source>
      <translation>in die Systemleiste verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQTrayIcon.qml" line="38"/>
      <source>show tray icon but don&apos;t hide to it</source>
      <translation>Symbol anzeigen aber nicht dahin verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="31"/>
      <source>window management</source>
      <extracomment>A settings title.</extracomment>
      <translation>Fensterverwaltung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="32"/>
      <source>Some basic window management properties.</source>
      <translation>Einige einfache Eigenschaften der Fensterverwaltung.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="40"/>
      <source>manage window through quick info labels</source>
      <translation>Fenster mit Schnellinfo-Labels verwalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="46"/>
      <source>save and restore window geometry</source>
      <translation>Fenstergeometrie speichern und wiederherstellen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowManagement.qml" line="51"/>
      <source>keep above other windows</source>
      <translation>über anderen Fenstern halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQOverlayColor.qml" line="54"/>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="31"/>
      <source>window mode</source>
      <extracomment>A settings title.</extracomment>
      <translation>Fenstermodus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="32"/>
      <source>Whether to run PhotoQt in window mode or fullscreen.</source>
      <translation>Ob PhotoQt im Fenstermodus oder Vollbild ausgeführt werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="40"/>
      <source>run in window mode</source>
      <translation>im Fenstermodus ausführen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQWindowMode.qml" line="45"/>
      <source>show window decoration</source>
      <translation>Fensterdekoration zeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="34"/>
      <source>custom context menu entries</source>
      <extracomment>A settings title.</extracomment>
      <translation>eigene Einträge im Kontextmenü</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="35"/>
      <source>Add some custom entries to the context menu.</source>
      <translation>Füge dem Kontextmenü benutzerdefinierte Einträge hinzu.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="58"/>
      <source>Set entries for other image related applications</source>
      <translation>Einträge für andere bildbezogene Anwendungen festlegen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="60"/>
      <source>This will look for some other image related applications on your computer and add an entry for any that are found.</source>
      <translation>Dies wird nach anderen Bildanwendungen auf dem Computer suchen und einen Eintrag für alle gefundenen hinzufügen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="61"/>
      <source>Note that this will replace all entries currently set and cannot be undone.</source>
      <translation>Beachte, dass dies alle aktuell gesetzten Einträge ersetzt und nicht rückgängig gemacht werden kann.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="111"/>
      <source>Click to change the icon of the entry</source>
      <translation>Hier klicken, um das Symbol des Eintrags zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="128"/>
      <source>what string to show for this entry</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation>welcher Text für diesen Eintrag angezeigt werden soll</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="150"/>
      <source>Click here to select an executable to be used with this shortcut.</source>
      <translation>Hier klicken, um eine ausführbare Datei auszuwählen, die mit dieser Verknüpfung verwendet werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="153"/>
      <source>executable</source>
      <extracomment>This is written on a button, used as in &apos;click this button to select an executable&apos;</extracomment>
      <translation>ausführbare Datei</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="174"/>
      <source>additional command line flags</source>
      <extracomment>this is the placeholder text inside of a text box telling the user what text they can enter here</extracomment>
      <translation>zusätzliche Flags für die Kommandozeile</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="198"/>
      <source>quit</source>
      <extracomment>Keep string short! Used on checkbox for contextmenu, refers to option to close PhotoQt after respective command has been executed.</extracomment>
      <translation>beenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="218"/>
      <source>move entry down</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry down in the list of all entries&apos;</extracomment>
      <translation>Eintrag nach unten verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="231"/>
      <source>move entry up</source>
      <extracomment>contextmenu settings: used as in &apos;move this entry up in the list of all entries&apos;</extracomment>
      <translation>Eintrag nach oben verschieben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="249"/>
      <source>delete entry</source>
      <extracomment>contextmenu settings: used as in &apos;delete this entry out of the list of all entries&apos;</extracomment>
      <translation>Eintrag löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQContextMenu.qml" line="277"/>
      <source>Also show entries in main menu</source>
      <translation>Einträge auch im Hauptmenü anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="31"/>
      <source>navigation buttons</source>
      <extracomment>A settings title.</extracomment>
      <translation>Knöpfe für die Navigation</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="32"/>
      <source>Some buttons to help with navigation. These can come in handy when, e.g., operating with a touch screen.</source>
      <translation>Einige Knöpfe um mit der Navigation zu helfen. Diese können hilfreich sein, wenn z.B. mit einem Touchscreen gearbeitet wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="41"/>
      <source>buttons next to window buttons</source>
      <translation>Knöpfe neben den Fensterknöpfen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQNavigation.qml" line="46"/>
      <source>floating buttons</source>
      <translation>Schwebende Knöpfe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="30"/>
      <source>popout when needed</source>
      <extracomment>A settings title.</extracomment>
      <translation>Pop-out bei Bedarf</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="31"/>
      <source>Some elements require a minimum amount of space. When the window is smaller than that, those elements can be automatically popped out to provide the space they need.</source>
      <translation>Einige Elemente haben einen minimalen Platzbedarf. Wenn das Fenster zu klein ist, dann können diese Elemente automatisch in ihrem eigenen Fenster angezeigt werden, damit sie ihren benötigten Platz haben.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/interface/PQPopoutWhenSmall.qml" line="36"/>
      <source>pop out when application window is small</source>
      <translation>Pop-out, wenn das Anwendungsfenster klein ist</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_metadata</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="31"/>
      <source>face tags</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Gesichter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="33"/>
      <source>Whether to show face tags (stored in metadata info).</source>
      <extracomment>The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Ob Gesichtsmarkierungen angezeigt werden sollen (in Metadaten gespeichert).</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTags.qml" line="38"/>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="37"/>
      <source>enable</source>
      <translation>aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="32"/>
      <source>face tags - border</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Gesichert - Rahmen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="33"/>
      <source>If and what style of border to show around tagged faces.</source>
      <translation>Ob und wie ein Rahmen um markierte Gesichter gezeigt werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="45"/>
      <source>show border</source>
      <extracomment>The border here is the border around face tags.</extracomment>
      <translation>Rahmen anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="96"/>
      <source>click to change color</source>
      <translation>klicke, um die Farbe zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsBorder.qml" line="110"/>
      <source>please choose a color</source>
      <translation>bitte eine Farbe wählen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="31"/>
      <source>face tags - font size</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Gesichter - Schriftgröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsFontSize.qml" line="33"/>
      <source>The font size of the name labels.</source>
      <extracomment>The name labels here are the labels with the name used for the face tags.</extracomment>
      <translation>Die Schriftgröße der Namensbeschriftungen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="31"/>
      <source>face tags - visibility</source>
      <extracomment>A settings title. The face tags are labels that can be shown (if available) on faces including their name.</extracomment>
      <translation>Gesichter - Sichtbarkeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="32"/>
      <source>When to show the face tags and for how long.</source>
      <translation>Wann und wie lange die Markierungen der Gesichter angezeigt werden sollen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="38"/>
      <source>hybrid mode</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>Hybrid-Modus</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="40"/>
      <source>always show all</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>immer alle anzeigen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="42"/>
      <source>show one on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>zeige Eine beim Überfahren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQFaceTagsVisibility.qml" line="44"/>
      <source>show all on hover</source>
      <extracomment>A mode for showing face tags.</extracomment>
      <translation>zeige Alle beim Überfahren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="31"/>
      <source>GPS online map</source>
      <extracomment>A settings title.</extracomment>
      <translation>GPS Onlinekarte</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQGPSMap.qml" line="32"/>
      <source>Which map service to use when a GPS position is clicked.</source>
      <translation>Welcher Kartendienst verwendet werden soll, wenn eine GPS-Position angeklickt wird.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="31"/>
      <source>meta information</source>
      <extracomment>A settings title.</extracomment>
      <translation>Metadaten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="32"/>
      <source>Which meta information to extract and display.</source>
      <translation>Welche Metadaten zu extrahieren und anzuzeigen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="35"/>
      <source>file name</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dateiname</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="37"/>
      <source>file type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dateityp</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="39"/>
      <source>file size</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Dateigröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="41"/>
      <source>image #/#</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Bild #/#</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="43"/>
      <source>dimensions</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Bildgröße</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="45"/>
      <source>copyright</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Copyright</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="47"/>
      <source>exposure time</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Belichtungszeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="49"/>
      <source>flash</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Blitz</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="51"/>
      <source>focal length</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Brennweite</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="53"/>
      <source>f-number</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Blendenzahl</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="55"/>
      <source>GPS position</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>GPS-Position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="58"/>
      <source>keywords</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Schlüsselwörter</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="60"/>
      <source>light source</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Lichtquelle</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="62"/>
      <source>location</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Standort</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="64"/>
      <source>make</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Hersteller</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="66"/>
      <source>model</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Modell</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="68"/>
      <source>scene type</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Szenenart</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="70"/>
      <source>software</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Software</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQMetaData.qml" line="72"/>
      <source>time photo was taken</source>
      <extracomment>Part of the meta information about the current image.</extracomment>
      <translation>Aufnahmezeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="30"/>
      <source>auto-rotation</source>
      <extracomment>A settings title.</extracomment>
      <translation>automatische Drehung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQRotation.qml" line="31"/>
      <source>Automatically rotate images based on metadata information.</source>
      <translation>Bilder auf Basis von Metadaten automatisch drehen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQBehindLeftEdge.qml" line="31"/>
      <source>how to access</source>
      <extracomment>A settings title.</extracomment>
      <translation>wie darauf zugreifen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/metadata/PQBehindLeftEdge.qml" line="32"/>
      <source>The meta data can be accessed in two different ways: 1) A floating element that can be moved freely, or 2) hidden behind the left screen edge and shown when the mouse cursor is moved there.</source>
      <translation>Auf die Metadaten können auf zwei verschiedene Arten zugegriffen werden: 1) Ein schwebendes Element, das frei bewegt werden kann, oder 2) versteckt hinter dem linken Bildschirmrand, und angezeigt, wenn der Mauszeiger sich dort bewegt wird.</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_shortcuts</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="61"/>
      <source>quit</source>
      <extracomment>checkbox in shortcuts settings, used as in: quit PhotoQt. Please keep as short as possible!</extracomment>
      <translation>beenden</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="96"/>
      <source>Click to change shortcut</source>
      <translation>Klicken, um Kurzbefehl zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="130"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="144"/>
      <source>Click to delete shortcut</source>
      <translation>Klicken, um Kurzbefehl zu löschen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalShortcutTile.qml" line="89"/>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="61"/>
      <source>no shortcut set</source>
      <translation>kein Kurzbefehl gesetzt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="71"/>
      <source>Click to manage shortcut</source>
      <translation>Klicke, um Kurzbefehl zu verwalten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQShortcutTile.qml" line="172"/>
      <source>add new</source>
      <extracomment>Used as in &apos;add new shortcut&apos;. Please keep short!</extracomment>
      <translation>Neuen hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="113"/>
      <source>Add New Shortcut</source>
      <translation>Neuen Kurzbefehl hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQNewShortcut.qml" line="134"/>
      <source>Perform a mouse gesture here or press any key combo</source>
      <translation>Führe hier eine Mausgeste aus oder drücke eine beliebige Tastenkombination</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/shortcuts/PQExternalContainer.qml" line="47"/>
      <source>Add new</source>
      <extracomment>Used on button as in &apos;add new external shortcut&apos;. Please keep short!</extracomment>
      <translation>Neuen hinzufügen</translation>
    </message>
  </context>
  <context>
    <name>settingsmanager_thumbnails</name>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="30"/>
      <source>thumbnail cache</source>
      <extracomment>A settings title.</extracomment>
      <translation>Zwischenspeicherung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="31"/>
      <source>Thumbnails can be cached (permanently), following the freedesktop.org standard.</source>
      <translation>Miniaturbilder können (dauerhaft) zwischengespeichert werden, dem Standard von freedesktop.org entsprechend.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCache.qml" line="36"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="45"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="46"/>
      <source>enable</source>
      <translation>aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="30"/>
      <source>keep in center</source>
      <extracomment>A settings title. Used as in: Keep thumbnail for current main image in center.</extracomment>
      <translation>in der Mitte halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="31"/>
      <source>Keep currently active thumbnail in the center of the screen</source>
      <translation>Aktuelles Miniaturbild in der Mitte des Bildschirms halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQCenter.qml" line="35"/>
      <source>center on active thumbnail</source>
      <translation>aktuelles Miniaturbild in der Mitte halten</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="29"/>
      <source>disable thumbnails</source>
      <translation>Miniaturbilder deaktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="30"/>
      <source>Disable thumbnails in case no thumbnails are desired whatsoever.</source>
      <translation>Miniaturbilder deaktivieren falls keine erwünscht sind.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQDisable.qml" line="35"/>
      <source>disable all thumbnails</source>
      <translation>alle Miniaturbilder deaktivieren</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="30"/>
      <source>filename label</source>
      <extracomment>A settings title. The filename label here is the one that is written on thumbnails.</extracomment>
      <translation>Dateinamenbeschriftung</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="31"/>
      <source>Show the filename on a small label on the thumbnail image.</source>
      <translation>Den Dateinamen als Beschriftung auf dem Miniaturbild anzeigen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameLabel.qml" line="57"/>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="58"/>
      <source>font size:</source>
      <translation>Schriftgröße:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="30"/>
      <source>filename-only</source>
      <extracomment>A settings title. This refers to using only the filename as thumbnail and no actual image.</extracomment>
      <translation>nur den Dateinamen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQFilenameOnly.qml" line="31"/>
      <source>Show only the filename as thumbnail, no actual image.</source>
      <translation>Zeige nur den Dateinamen als Miniaturbild, nicht das eigentliche Bild.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="30"/>
      <source>lift up</source>
      <extracomment>A settings title. This refers to the lift up of thumbnail images when active/hovered.</extracomment>
      <translation>Anheben</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQLiftUp.qml" line="31"/>
      <source>How many pixels to lift up thumbnails when either hovered or active.</source>
      <translation>Wie viele Pixel ein Miniaturbild angehoben werden sollen falls der Mauszeiger sich darüber befindet oder es aktiv ist.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="30"/>
      <source>position</source>
      <extracomment>A settings title referring to the position of the thumbnails (upper or lower edge of PhotoQt).</extracomment>
      <translation>Position</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="31"/>
      <source>Which edge to show the thumbnails on, upper or lower edge.</source>
      <translation>An welcher Bildschirmkante die Miniaturbilder angezeigt werden sollen, oben oder unten.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="38"/>
      <source>upper edge</source>
      <extracomment>The upper edge of PhotoQt</extracomment>
      <translation>obere Bildschirmkante</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQPosition.qml" line="40"/>
      <source>lower edge</source>
      <extracomment>The lower edge of PhotoQt</extracomment>
      <translation>untere Bildschirmkante</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="30"/>
      <source>size</source>
      <extracomment>A settings title referring to the size of the thumbnails.</extracomment>
      <translation>Größe</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSize.qml" line="31"/>
      <source>How large (or small) the thumbnails should be.</source>
      <translation>Wie groß (oder klein) die Miniaturbilder sein sollen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="30"/>
      <source>spacing</source>
      <extracomment>A settings title referring to the spacing of thumbnails, i.e., how much empty space to have between each.</extracomment>
      <translation>Abstand</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQSpacing.qml" line="31"/>
      <source>How much space to show between the thumbnails.</source>
      <translation>Wie viel Platz zwischen den Miniaturbildern angezeigt werden soll.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="30"/>
      <source>threads</source>
      <extracomment>A settings title, as in: How many threads to use to generate thumbnails.</extracomment>
      <translation>Threads</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="31"/>
      <source>How many threads to use to create thumbnails. Too many threads can slow down your computer!</source>
      <translation>Wie viele Threads zum Erstellen von Miniaturbildern verwendet werden sollen. Zu viele Threads können den Computer verlangsamen!</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQThreads.qml" line="50"/>
      <source>Threads:</source>
      <translation>Threads:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="30"/>
      <source>visibility</source>
      <extracomment>A settings title referring to the visibility of the thumbnails, i.e., if and when to hide them.</extracomment>
      <translation>Sichtbarkeit</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="31"/>
      <source>If and how to keep thumbnails visible</source>
      <translation>Wenn und wie die Miniaturbilder sichtbar gehalten werden sollen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="37"/>
      <source>hide when not needed</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>verstecken wenn nicht benötigt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="39"/>
      <source>never hide</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>nie verstecken</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQVisible.qml" line="41"/>
      <source>hide when zoomed in</source>
      <extracomment>This is talking about the thumbnails.</extracomment>
      <translation>verstecken wenn gezoomt</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="29"/>
      <source>exclude folders</source>
      <translation>ausgeschlossene Verzeichnisse</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="30"/>
      <source>Exclude the specified folders and all of its subfolders from any sort of caching and preloading.</source>
      <translation>Die angegebenen Ordner und alle Unterordner von jeder Art des Zwischenspeicherns und des Vorladens ausschließen.</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="103"/>
      <source>Do not cache these folders:</source>
      <translation>Diese Ordner nicht zwischenspeichern:</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="115"/>
      <source>Add folder</source>
      <extracomment>Written on a button</extracomment>
      <translation>Ordner hinzufügen</translation>
    </message>
    <message>
      <location filename="../qml/settingsmanager/tabs/thumbnails/PQExcludeFolders.qml" line="110"/>
      <source>One folder per line</source>
      <translation>Ein Ordner pro Zeile</translation>
    </message>
  </context>
  <context>
    <name>slideshow</name>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="115"/>
      <source>Click to go to the previous image</source>
      <translation>Klicken, um zum vorherigen Bild zu gehen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="147"/>
      <source>Click to pause slideshow</source>
      <translation>Klicken, um die Diaschau zu pausieren</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="148"/>
      <source>Click to play slideshow</source>
      <translation>Klicken, um die Diaschau zu starten</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="176"/>
      <source>Click to go to the next image</source>
      <translation>Klicke, um zum nächsten Bild zu gehen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="206"/>
      <source>Click to exit slideshow</source>
      <translation>Klicken, um die Diaschau zu beenden</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControls.qml" line="256"/>
      <source>Sound volume:</source>
      <translation>Lautstärke:</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="61"/>
      <location filename="../qml/slideshow/PQSlideShowSettingsPopout.qml" line="34"/>
      <source>Slideshow settings</source>
      <extracomment>Window title</extracomment>
      <translation>Einstellungen für die Diaschau</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="110"/>
      <source>interval</source>
      <extracomment>The interval between images in a slideshow</extracomment>
      <translation>Zeitintervall</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="147"/>
      <source>animation</source>
      <extracomment>This is referring to the in/out animation of images during a slideshow</extracomment>
      <translation>Animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="159"/>
      <source>opacity</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>Deckkraft</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="161"/>
      <source>along x-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>entlang der x-Achse</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="163"/>
      <source>along y-axis</source>
      <extracomment>This is referring to the in/out animation of images during slideshows</extracomment>
      <translation>entlang der y-Achse</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="165"/>
      <source>rotation</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Drehung</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="167"/>
      <source>explosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Explosion</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="169"/>
      <source>implosion</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>Implosion</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="171"/>
      <source>choose one at random</source>
      <extracomment>This is referring to the in/out animation of images</extracomment>
      <translation>zufällig auswählen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="191"/>
      <source>animation speed</source>
      <extracomment>The speed of transitioning from one image to another during slideshows</extracomment>
      <translation>Animationsgeschwindigkeit</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="211"/>
      <source>immediately, without animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>sofort, ohne Animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="214"/>
      <source>pretty fast animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>ziemlich schnelle Animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="217"/>
      <source>not too fast and not too slow</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>nicht zu schnell und nicht zu langsam</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="219"/>
      <source>very slow animation</source>
      <extracomment>This refers to a speed of transitioning from one image to another during slideshows</extracomment>
      <translation>sehr langsame Animation</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="226"/>
      <source>current speed</source>
      <extracomment>This refers to the currently set speed of transitioning from one image to another during slideshows</extracomment>
      <translation>aktuelle Geschwindigkeit</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="248"/>
      <source>looping</source>
      <translation>Wiederholen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="261"/>
      <source>loop over all files</source>
      <extracomment>Loop over all images during slideshows</extracomment>
      <translation>alle Dateien in Dauerschleife durchlaufen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="278"/>
      <source>shuffle</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>Zufallswiedergabe</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="291"/>
      <source>shuffle all files</source>
      <extracomment>during slideshows shuffle the order of all images</extracomment>
      <translation>alle Dateien mischen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="308"/>
      <source>subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>Unterordner</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="321"/>
      <source>include images in subfolders</source>
      <extracomment>also include images in subfolders during slideshows</extracomment>
      <translation>Bilder aus den Unterordnern mit einbeziehen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="339"/>
      <source>file info</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>Datei-Informationen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="352"/>
      <source>hide label with details about current file</source>
      <extracomment>What to do with the file details during slideshows</extracomment>
      <translation>die Labels mit Details zur aktuellen Datei ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="370"/>
      <source>window buttons</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>Fensterknöpfe</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="383"/>
      <source>hide window buttons during slideshow</source>
      <extracomment>What to do with the window buttons during slideshows</extracomment>
      <translation>Fensterknöpfe während der Slideshow ausblenden</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="401"/>
      <source>music</source>
      <extracomment>The music that is to be played during slideshows</extracomment>
      <translation>Musik</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="418"/>
      <source>enable music</source>
      <extracomment>Enable music to be played during slideshows</extracomment>
      <translation>Musik aktivieren</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="425"/>
      <source>no file selected</source>
      <translation>keine Datei ausgewählt</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="427"/>
      <source>Click to select music file</source>
      <translation>Klicken, um Musikdatei auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="428"/>
      <source>Click to change music file</source>
      <translation>Klicken, um Musikdatei zu ändern</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="439"/>
      <source>Common music file formats</source>
      <translation>Verbreitete Musikdateiformate</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="440"/>
      <source>All Files</source>
      <translation>Alle Dateien</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowSettings.qml" line="475"/>
      <source>Start slideshow</source>
      <extracomment>Written on a clickable button</extracomment>
      <translation>Diaschau starten</translation>
    </message>
    <message>
      <location filename="../qml/slideshow/PQSlideShowControlsPopout.qml" line="34"/>
      <source>Slideshow controls</source>
      <extracomment>Window title</extracomment>
      <translation>Diaschau - Steuerelemente</translation>
    </message>
  </context>
  <context>
    <name>startup</name>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="330"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="332"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="334"/>
      <source>Edit with %1</source>
      <extracomment>Used as in &apos;Edit with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Mit %1 bearbeiten</translation>
    </message>
    <message>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="336"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="338"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="340"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="342"/>
      <location filename="../cplusplus/scripts/handlingexternal.cpp" line="344"/>
      <source>Open in %1</source>
      <extracomment>Used as in &apos;Open with [application]&apos;. %1 will be replaced with application name.</extracomment>
      <translation>Mit %1 öffnen</translation>
    </message>
  </context>
  <context>
    <name>streaming</name>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="98"/>
      <source>Scan for devices</source>
      <extracomment>Used as tooltip for button that starts a scan for Chromecast streaming devices in the local network</extracomment>
      <translation>Nach Geräten suchen</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="206"/>
      <source>Disconnect</source>
      <extracomment>Written on button, as in &apos;Disconnect from connected Chromecast streaming device&apos;</extracomment>
      <translation>Trennen</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="208"/>
      <source>Connect</source>
      <extracomment>Written on button, as in &apos;Connect to Chromecast streaming device&apos;</extracomment>
      <translation>Verbinden</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="241"/>
      <source>searching for devices...</source>
      <extracomment>status text while searching for chromecast streaming devices in the local network</extracomment>
      <translation>Geräte werden gesucht...</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecast.qml" line="243"/>
      <source>no devices found</source>
      <extracomment>result of scan for chromecast streaming devices</extracomment>
      <translation>keine Geräte gefunden</translation>
    </message>
    <message>
      <location filename="../qml/chromecast/PQChromecastPopout.qml" line="34"/>
      <source>Streaming (Chromecast)</source>
      <extracomment>Window title</extracomment>
      <translation>Streaming (Chromecast)</translation>
    </message>
  </context>
  <context>
    <name>thumbnailbar</name>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="269"/>
      <source>File size:</source>
      <translation>Dateigröße:</translation>
    </message>
    <message>
      <location filename="../qml/mainwindow/PQThumbnailBar.qml" line="270"/>
      <source>File type:</source>
      <translation>Dateityp:</translation>
    </message>
  </context>
  <context>
    <name>unavailable</name>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="75"/>
      <source>Sorry, but this feature is not yet available on Windows.</source>
      <translation>Leider ist diese Funktion noch nicht unter Windows verfügbar.</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailablePopout.qml" line="34"/>
      <source>Feature unavailable</source>
      <extracomment>Window title, informing user that the requested feature is currently not available</extracomment>
      <translation>Funktion nicht verfügbar</translation>
    </message>
    <message>
      <location filename="../qml/unavailable/PQUnavailable.qml" line="74"/>
      <source>Sorry, but this feature is not available.</source>
      <translation>Diese Funktion ist leider nicht verfügbar.</translation>
    </message>
  </context>
  <context>
    <name>wallpaper</name>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="71"/>
      <source>Warning: %1 module not activated</source>
      <translation>Warnung: Modul %1 nicht aktiviert</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="80"/>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="69"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="72"/>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="81"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="69"/>
      <source>Warning: %1 not found</source>
      <translation>Warnung: %1 nicht gefunden</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="102"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="91"/>
      <source>Set to which screens</source>
      <extracomment>As in: Set wallpaper to which screens</extracomment>
      <translation>Für welche Bildschirme setzen</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="115"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="103"/>
      <source>Screen</source>
      <extracomment>Used in wallpaper element</extracomment>
      <translation>Bildschirm</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="140"/>
      <source>Set to which workspaces</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Für welche Arbeitsflächen setzen</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQEnlightenment.qml" line="154"/>
      <source>Workspace:</source>
      <extracomment>Enlightenment desktop environment handles wallpapers per workspace (different from screen)</extracomment>
      <translation>Arbeitsfläche:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQGnome.qml" line="83"/>
      <location filename="../qml/wallpaper/ele/PQWindows.qml" line="66"/>
      <location filename="../qml/wallpaper/ele/PQXfce.qml" line="128"/>
      <source>Choose picture option</source>
      <extracomment>picture option refers to how to format a pictrue when setting it as wallpaper</extracomment>
      <translation>Bildoption auswählen</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="57"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="180"/>
      <source>Other</source>
      <extracomment>Used as in: Other Desktop Environment</extracomment>
      <translation>Andere</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQOther.qml" line="100"/>
      <source>Tool:</source>
      <extracomment>Tool refers to a program that can be executed</extracomment>
      <translation>Werkzeug:</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="106"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="124"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="142"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="160"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="178"/>
      <source>Click to choose %1</source>
      <extracomment>%1 is a placeholder for the name of a desktop environment (plasma, xfce, gnome, etc.)</extracomment>
      <translation>Klicken, um %1 auszuwählen</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="207"/>
      <location filename="../qml/wallpaper/PQWallpaper.qml" line="226"/>
      <location filename="../qml/wallpaper/PQWallpaperPopout.qml" line="34"/>
      <source>Set as Wallpaper</source>
      <extracomment>Heading of wallpaper element
----------
Written on clickable button
----------
Window title</extracomment>
      <translation>Als Hintergrund setzen</translation>
    </message>
    <message>
      <location filename="../qml/wallpaper/ele/PQPlasma.qml" line="67"/>
      <source>The image will be set to all screens at the same time.</source>
      <translation>Das Bild wird auf alle Bildschirme gleichzeitig gesetzt.</translation>
    </message>
  </context>
</TS>
