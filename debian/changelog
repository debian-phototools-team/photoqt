photoqt (3.0-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: bump years.
  * d/control:
    - add qml-module-qt-labs-platform to Depends.
    - add kimageformats-plugins to Recommends.

 -- Gürkan Myczko <tar@debian.org>  Wed, 01 Feb 2023 07:57:52 +0100

photoqt (2.9.1-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 05 Sep 2022 21:08:44 +0200

photoqt (2.8-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Wed, 03 Aug 2022 16:20:44 +0200

photoqt (2.7-1) unstable; urgency=medium

  * New upstream version.
  * d/upstream/metadata: added.
  * Bump standards version to 4.6.1.

 -- Gürkan Myczko <tar@debian.org>  Sun, 12 Jun 2022 08:05:28 +0200

photoqt (2.6-1) unstable; urgency=medium

  * New upstream version.
  * Patched upstream CMakeLists.txt to not do the pychromecast check.

 -- Gürkan Myczko <tar@debian.org>  Fri, 08 Apr 2022 08:55:33 +0200

photoqt (2.5-5) unstable; urgency=medium

  * d/control: improve arch exceptions for pychromecast.

 -- Gürkan Myczko <tar@debian.org>  Sat, 26 Mar 2022 17:06:50 +0100

photoqt (2.5-4) unstable; urgency=medium

  * d/control: add missing build-depends for graphicsmagick.

 -- Gürkan Myczko <tar@debian.org>  Fri, 25 Mar 2022 20:55:55 +0100

photoqt (2.5-3) unstable; urgency=medium

  * Fix some FTBFS problems, thanks to Lukas Spies and Adrian Bunk:
    - d/rules: adapt cmake options depending on architecture.
    - d/control: limit build-depends for arch.

 -- Gürkan Myczko <tar@debian.org>  Wed, 23 Mar 2022 19:41:09 +0100

photoqt (2.5-2) unstable; urgency=medium

  * d/control: add python3-pychromecast to Depends.
  * Source only upload.

 -- Gürkan Myczko <tar@debian.org>  Wed, 23 Mar 2022 05:58:50 +0100

photoqt (2.5-1) unstable; urgency=medium

  * Initial release. (Closes: #1008066)

 -- Gürkan Myczko <tar@debian.org>  Thu, 17 Mar 2022 12:39:25 +0100
